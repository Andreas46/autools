﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using AuTools;

[System.Serializable]
abstract public class SaveObjData
{
	public string implName;
}

abstract public class SaveObj : MonoBehaviour
{
	static public List <SaveObj> s_savables = new List <SaveObj> ();

	static public List <SaveObjData> s_savedObjData = new List <SaveObjData>();

//	static MemoryStream getCurrentGameState ()
//	{
//		BinaryFormatter formatter = new BinaryFormatter();
//		MemoryStream retStream = new MemoryStream();
//
//		formatter.Serialize (retStream, s_savables);
//	
//		return retStream;
//	}

	static public void saveAllObjProperties  (Vector3 savePos)
	{
		s_savedObjData.Clear();

		foreach (SaveObj sObj in s_savables)
		{
			// Get a snapshot of this object's data.
			SaveObjData data = sObj.saveData (savePos);

			// Set the id of this saveable object.
			//print (sObj.getObjPath());
			data.implName = sObj.getObjPath();

			s_savedObjData.Add (data);
		}
	}

	static public void restoreAllObjProperties ()
	{
		Debug.Log ("Restoring all properties (" + s_savables.Count + ").");
		foreach (SaveObj sObj in s_savables)
		{
			string sObjPath = sObj.getObjPath();

			// Find the Object data belonging to this object.
			foreach (SaveObjData sData in s_savedObjData)
			{
				if (sData.implName == sObjPath)
				{
					//sObj.setSaveData (sData);
					sObj.restoreData (sData);
				}
			}

			//sObj.restoreProperties();
		}
	}

	// Use this for initialization
	protected void Awake () 
	{
		s_savables.Add (this);
	}

	protected void OnDestroy ()
	{
		s_savables.Remove (this);
	}
	
//	// Update is called once per frame
//	protected void Update ()
//	{
//	
//	}

	protected abstract SaveObjData saveData (Vector3 savePos);
	protected abstract void restoreData (SaveObjData data);

//	protected abstract SaveObjData getSaveData ();
//	protected abstract void setSaveData (SaveObjData data);

	//protected abstract void serialise (B)
}
