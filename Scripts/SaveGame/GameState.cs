﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

//using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Formatters.Binary;

public class GameState
{
	static MemoryStream s_lastGameSnapshot;

	static public void saveGame (Vector3 savePos, string saveFilePath = "")
	{
		SaveObj.saveAllObjProperties (savePos);

		s_lastGameSnapshot = getCurrentGameState();

		if (saveFilePath != "")
		{
			s_lastGameSnapshot.Seek (0, SeekOrigin.Begin);  // Reset read position.

			FileStream fStream = new FileStream (saveFilePath, FileMode.Create);
			s_lastGameSnapshot.WriteTo (fStream);
		}
	}

	static public void restoreGame (string saveFilePath = "")
	{
		if (saveFilePath != "" && File.Exists (saveFilePath))
		{
			s_lastGameSnapshot = new MemoryStream();
			FileStream fStream = new FileStream (saveFilePath, FileMode.Open);

			byte[] bytes = new byte [fStream.Length];
			fStream.Read (bytes, 0, (int) fStream.Length);

			s_lastGameSnapshot.Write (bytes, 0, (int) fStream.Length);
			fStream.Close();
		}

		if (s_lastGameSnapshot != null)
		{
			s_lastGameSnapshot.Seek (0, SeekOrigin.Begin);
			setGameState (s_lastGameSnapshot);

			SaveObj.restoreAllObjProperties();
		}
	}

	static public bool isGameSaved()
	{
		if (s_lastGameSnapshot != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	static public bool isGameSavedToDisk (string filePath)
	{
		if (File.Exists (filePath))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	static MemoryStream getCurrentGameState ()
	{
		BinaryFormatter formatter = new BinaryFormatter();
		MemoryStream retStream = new MemoryStream();
		
		formatter.Serialize (retStream, SaveObj.s_savedObjData);
		
		return retStream;
	}

	static void setGameState (Stream stream)
	{
		BinaryFormatter formatter = new BinaryFormatter();

		SaveObj.s_savedObjData = (List <SaveObjData>) formatter.Deserialize (stream);
	}
}
