﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class  SaveDataTransform: SaveObjData
{
	public SerializableVector3 position;
	public SerializableQuaternion rotation;
	public SerializableVector3 scale;
}

public class SaveTransform : SaveObj 
{

	protected override SaveObjData saveData (Vector3 savePos)
	{
		SaveDataTransform data = new SaveDataTransform ();

		data.position = transform.position;
		data.rotation = transform.rotation;
		data.scale = transform.localScale;

		return data;
	}
	
	protected override void restoreData (SaveObjData data)
	{
		SaveDataTransform transData = data as SaveDataTransform;

		transform.position = transData.position;
		transform.rotation = transData.rotation;
		transform.localScale = transData.scale;
	}
}
