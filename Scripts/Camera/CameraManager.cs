﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;

// Replace "CustomInspectorTemplate"
// Replace "BeingEdited" with the class to be edited.

[CustomEditor (typeof (CameraManager))]
[CanEditMultipleObjects]
public class CustomInspectorTemplate : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		// Horizontal rule.
		GUILayout.Box("", new GUILayoutOption[]{GUILayout.ExpandWidth(true), GUILayout.Height(1)});

		if (!Application.isPlaying)
		{
			return;
		}

		GUILayout.BeginHorizontal();

		if (targets.Length == 1)
		{
			CameraManager script = target as CameraManager;

			if (script.isActive ())
			{
				//GUILayoutOption option = new GUILayoutOption ();

				GUILayout.Label ("Active");
			}
			else
			{
				GUILayout.Label ("Inactive");
			}

			if (GUILayout.Button ("SwitchTo"))
			{
				//foreach (BeingEdited script in targets)
				//{
				script.switchTo();
				//}
			}
		}

		GUILayout.EndHorizontal();
	}

}

#endif

[DisallowMultipleComponent]
[RequireComponent (typeof (Camera))]
public class CameraManager : MonoBehaviour
{

    static CameraManager m_activeCamera;
	static CameraManager m_defaultCamera;

    static public Camera mainCamera
    {
        get
		{
			if (m_activeCamera != null)
			{
				return m_activeCamera.m_attachedCam;
			}
			else if (m_defaultCamera != null)
			{
				return m_defaultCamera.m_attachedCam;
			}
			else
			{
				// Fall back to camera.main.
				return Camera.main;
			}	
		}
    }

	static public CameraManager mainCamMgr { get { return m_activeCamera; } }

    static HashSet <CameraManager> s_allCameras = new HashSet<CameraManager>();

    protected Camera m_attachedCam;
	public Camera AttachedCam { get {return m_attachedCam; } }

    public bool m_startEnabled = false;
	public bool m_isDefaultCam = false;

	public static CameraManager newCamMgr (bool activate = false)
	{
		GameObject newCam = new GameObject();

		newCam.AddComponent<Camera> ();
		CameraManager camMgr = newCam.AddComponent<CameraManager> ();

		if (activate)
		{
			camMgr.switchTo ();
		}
		else
		{

		}

		return camMgr;
	}

	void OnReload (Camera cam)
	{

	}

	void Awake ()
	{
		s_allCameras.Add (this);

		m_attachedCam = GetComponent<Camera> ();

		if (m_attachedCam == null)
		{
			Debug.LogError ("Can't get camera component!?");
		}

		if (m_startEnabled)
		{
			enable();
		}
		else
		{
			disable();

			if (m_activeCamera)
			{
				// Disable audio listener.
				disableAudioListener();
			}
		}

		if (m_isDefaultCam)
		{
			m_defaultCamera = this;
		}
	}

	// Use this for initialization
	void Start () 
	{
//	        if (m_startEnabled)
//	        {
//	            switchTo();
//	        }
	}

	void OnDestroyed ()
	{
		if (m_activeCamera == this && m_defaultCamera != null)
		{
			switchToDefault ();
		}
	}

	public void switchTo ()
	{
		disableAllCamera();
		enable();
	}

	public static void switchToDefault ()
	{
		if (m_defaultCamera)
		{
			m_defaultCamera.switchTo ();
		}
		else
		{
			Debug.LogError ("No default camera set!");
		}
	}

	public void disable ()
	{
		//m_attachedCam.enabled = false;
		GetComponent<Camera> ().enabled = false;
	}

	protected void disableAudioListener()
	{
		AudioListener listener = GetComponent <AudioListener> ();
		if (listener)
		{
			listener.enabled = false;
		}
	}

	public void enable ()
	{
		m_activeCamera = this;

		GetComponent<Camera> ().enabled = true;

		// Enable audio Listener.
		AudioListener listener = GetComponent <AudioListener> ();
		if (listener)
		{
			// We're only disabling other listeners, if we have one to switch to.
			disableAllAudioListeners();

			// Enable this listener.
			listener.enabled = true;
		}
	}

	static void disableAllCamera ()
	{
		sanitiseAllCameras ();

		foreach (CameraManager cam in s_allCameras)
		{
		    cam.disable();
		}
	}

	static void disableAllAudioListeners ()
	{
		sanitiseAllCameras ();

		foreach (CameraManager cam in s_allCameras)
		{
			cam.disableAudioListener();
		}
	}

	static void sanitiseAllCameras ()
	{
		HashSet <CameraManager> newManagers = new HashSet <CameraManager>();

		foreach (CameraManager camManager in s_allCameras)
		{
		    if (camManager)
		    {
		        newManagers.Add(camManager);
		    }
		}

		s_allCameras = newManagers;
	}

	public static CameraManager findCamera (string camName)
	{
		sanitiseAllCameras();

		foreach (CameraManager camManager in s_allCameras)
		{
		    if (camManager.name == camName)
		    {
		        return camManager;
		    }
		}

		// Didn't find the camera by that name.
		return null;
	}

	public bool isActive ()
	{
		if (m_activeCamera != null && this == m_activeCamera)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
