﻿using UnityEngine;
using System.Collections;

public class CamLoading : MonoBehaviour {

	[Tooltip("The Camera to Switch to when Loading is complete.")]
	public CameraManager m_nextCam;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_nextCam)
		{
			m_nextCam.switchTo();
		}

		// We no longer need this script.
		//gameObject.SetActive (false);
	}
}
