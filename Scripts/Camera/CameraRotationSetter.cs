﻿using UnityEngine;
using System.Collections;

using AuTools;

public class CameraRotationSetter : MonoBehaviour
{
	public float m_targetCamRotation;
	
	SmoothObjMover m_mover;

	// Use this for initialization
	void Start ()
	{
		m_mover = SmoothObjMover.getObjMover (GameObject.Find ("CameraMount"));
		m_mover.m_travelTime = 1.0f;
	}

	void OnTriggerExit (Collider other)
	{

		if (other.gameObject.layer == LayerMask.NameToLayer ("Player"))
		{
			m_mover.beginTravel (m_mover.transform.position, Quaternion.AngleAxis (m_targetCamRotation, Vector3.up));
		}
	}
}
