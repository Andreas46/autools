﻿using UnityEngine;
using System.Collections;

public class CamCraneTrigger : MonoBehaviour {

	public CamCraneRig m_craneRig;

	[HideInInspector]
	public bool m_hasTriggered = false;

	public bool m_oneShot = false;
	public float m_travelTime = 0.0f;
	
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player")
		{
			activeCraneCam ();
		}
	}

	void OnCollisionEnter (Collision collision)
	{
		if (collision.collider.tag == "Player")
		{
			activeCraneCam ();
		}
	}

	void activeCraneCam ()
	{
		if (m_hasTriggered == false || m_oneShot == false)
		{
			if (m_travelTime > 0.0f)
			{
				m_craneRig.m_travelTime = m_travelTime;
			}

			m_craneRig.activateFromCurrentCamera ();
			m_hasTriggered = true;
		}
	}
}
