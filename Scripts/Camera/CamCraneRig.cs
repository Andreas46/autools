using UnityEngine;
using System.Collections.Generic;

using UnityEngine.Events;

using AuTools;

public class CamEvent: UnityEvent <CameraManager> {}

//[RequireComponent (typeof (SmoothObjMover))]
public class CamCraneRig : MonoBehaviour 
{
	public CameraManager m_cam;
	public CameraManager AttachedCam { get { return m_cam; } }

	//Vector3 m_startPos;
	//Quaternion m_startRot;

	public float m_travelTime = 2.0f;
	public float TravelTime { get { return m_travelTime; } set { m_travelTime = value; }}

	public bool m_useIntermediateCam = true;

	static Transform s_intCamParent = null;

	public CamEvent m_onCamChangeEvent = new CamEvent();

	CameraManager m_startCam;
	CameraManager m_targetCam;
	CameraManager m_intermediateCam;

	//static CamCraneRig s_currentCrane = null;
	static List <CamCraneRig> s_activeCraneRigs = new List<CamCraneRig> ();

	void Awake ()
	{
		if (m_cam == null)
		{
			m_cam = GetComponent<CameraManager> ();

			Debug.Assert (m_cam != null, "No Camera manager attached!");
		}

		if (m_useIntermediateCam)
		{
			if (s_intCamParent == null)
			{
				s_intCamParent = new GameObject ().transform;
				s_intCamParent.name = "IntermediateCameras";
			}

			m_intermediateCam = CameraManager.newCamMgr ();
			m_intermediateCam.name = "Intermediate Camera";

			m_intermediateCam.transform.parent = s_intCamParent;
		}
	}

	// Use this for initialization
	void OnDestroyed () 
	{
		if (m_intermediateCam != null)
		{
			Destroy (m_intermediateCam.gameObject);
		}

		if (s_activeCraneRigs.Contains (this))
		{
			s_activeCraneRigs.Remove (this);
		}
	}

	public void activateFromCurrentCamera()
	{
		activateFromCamera (CameraManager.mainCamMgr);
	}

	public CameraManager activateFromCamera (CameraManager cam)
	{
		m_targetCam = m_cam;
		m_startCam = cam;

		if (m_startCam == null || m_startCam == m_targetCam)
		{
			// This state occurs when the scene has not yet warmed up.
			// We'll skip the "Move" in this case.
			//			Debug.Log ("Skipping crain!");
			m_targetCam.enable();
			return m_targetCam;
		}

		//Vector3 startPos = 

		for (int i = 0; i < s_activeCraneRigs.Count; ++i)
		{
			// Does the camera in this rig correspond to the viewport of this cam?
			if (s_activeCraneRigs [i].m_cam.AttachedCam.rect == m_cam.AttachedCam.rect)
			{
				CamCraneRig activeRig = s_activeCraneRigs [i];

				if (activeRig.m_useIntermediateCam &&
					activeRig.m_intermediateCam != null)
				{
					// Change start cam to the activated int cam.
					m_startCam = s_activeCraneRigs [i].m_intermediateCam;
				}

				activeRig.cancelSwitch ();

				i--;
			}
		}

//		Vector3 targetPos = m_targetCam.transform.position;
//		Quaternion targetRotation = m_targetCam.transform.rotation;
//
//		if (m_useIntermediateCam)
//		{
//			// Set up correct view port for intermediate cam.
//			m_intermediateCam.AttachedCam.rect = m_startCam.AttachedCam.rect;
//
//			//objMover.m_travelTarget = m_intermediateCam.transform;
//			m_targetCam = m_intermediateCam;
//
//			s_activeCraneRigs.Add (this);
//
//			Invoke ("switchToTargetCam", m_travelTime);
//		}
//
//		SmoothObjMover objMover = SmoothObjMover.getObjMover (m_targetCam.gameObject);
//
//		// Travel time.
//		objMover.m_travelTime = m_travelTime;
//
//		if (m_useIntermediateCam)
//		{
//			objMover.m_travelTarget = m_cam.transform;
//		}
//
//		// Move camera to it's starting position.
//		m_targetCam.transform.position = m_startCam.transform.position;
//		m_targetCam.transform.rotation = m_startCam.transform.rotation;
//
//		// Begin movement.
//		objMover.beginTravel (targetPos, targetRotation);

		// Switch to camera.
		m_startCam.disable();
		//m_targetCam.enable();

		return activateFromPos (m_startCam.transform.position, m_startCam.transform.rotation,
			m_targetCam.transform.position, m_targetCam.transform.rotation, m_startCam);

		//return m_targetCam;
	}

	public CameraManager activateFromPos (Vector3 fromPos,
		Quaternion fromRot, CameraManager prevCam = null)
	{
		return activateFromPos (fromPos, fromRot,
			m_targetCam.transform.position, m_targetCam.transform.rotation, prevCam);
	}

	public CameraManager activateFromPos (Vector3 fromPos,
		Quaternion fromRot, Vector3 toPos, Quaternion toRot, CameraManager prevCam = null)
	{
		m_targetCam = m_cam;
		m_startCam = prevCam;

		// Check all currently active crane rigs.
		for (int i = 0; i < s_activeCraneRigs.Count; ++i)
		{
			// Cancel all current switches with the same viewport as this camera.
			if (s_activeCraneRigs [i].m_cam.AttachedCam.rect == m_cam.AttachedCam.rect)
			{
				s_activeCraneRigs [i].cancelSwitch ();
				i--;
			}
		}

		//if (m_startCam == null || m_startCam == m_targetCam)
		//{
		//	// This state occurs when the scene has not yet warmed up.
		//	// We'll skip the "Move" in this case.
		//	//m_targetCam.switchTo ();
		//
		//	//			Debug.Log ("Skipping crain!");
		//	m_targetCam.enable();
		//	return m_targetCam;
		//}

		Vector3 targetPos = toPos;
		Quaternion targetRotation = toRot;

		if (m_useIntermediateCam)
		{
			if (prevCam)
			{
				// Set up correct view port for intermediate cam.
				m_intermediateCam.AttachedCam.rect = prevCam.AttachedCam.rect;
			}
			else
			{
				m_intermediateCam.AttachedCam.rect = m_cam.AttachedCam.rect;
			}
				
			m_targetCam = m_intermediateCam;

			s_activeCraneRigs.Add (this);
			//s_currentCrane = this;
			Invoke ("switchToTargetCam", m_travelTime);
		}

		SmoothObjMover objMover = SmoothObjMover.getObjMover (m_targetCam.gameObject);

		// Travel time.
		objMover.m_travelTime = m_travelTime;

		if (m_useIntermediateCam)
		{
			objMover.m_travelTarget = m_cam.transform;
		}

		// Move camera to it's starting position.
		m_targetCam.transform.position = fromPos;
		m_targetCam.transform.rotation = fromRot;

		// Begin movement.
		objMover.beginTravel (targetPos, targetRotation);

		// Switch to camera.
		//m_startCam.disable();
		m_targetCam.enable();

		return m_targetCam;
	}

	public void cancelSwitch ()
	{
		CancelInvoke ();

		m_onCamChangeEvent.RemoveAllListeners ();

		//if (m_startCam != null)
		//{
		//	m_startCam.disable ();
		//}

		m_intermediateCam.disable ();

		s_activeCraneRigs.Remove (this);

		//if (s_currentCrane == this)
		//{
		//	s_currentCrane = null;
		//}
	}

	void switchToTargetCam ()
	{
		//m_cam.switchTo ();
		if (m_startCam != null)
		{
			m_startCam.disable ();
		}
		m_intermediateCam.disable ();
		m_cam.enable ();

		//if (s_currentCrane == this)
		//{
		//	s_currentCrane = null;
		//}

		s_activeCraneRigs.Remove (this);

		m_onCamChangeEvent.Invoke (m_cam);
		m_onCamChangeEvent.RemoveAllListeners ();
	}
}

