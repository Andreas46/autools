﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

public class EditPrefs : EditorWindow
{
    [MenuItem ("Window/SaveEditorPrefs")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
		EditPrefs window = (EditPrefs) EditorWindow.GetWindow (typeof (EditPrefs));
        window.titleContent = new GUIContent ("Save Editor Prefs");
        window.Show();
    }

	EditPrefs()
    {
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }

    void OnSceneGUI (SceneView sceneView)
    {

    }

    void OnGUI()
    {
        BeginWindows();

      //  m_ignoreTriggers = GUILayout.Toggle(m_ignoreTriggers, "Ignore Triggers");

        GUILayout.BeginHorizontal();

        if (GUILayout.Button ("Save Prefs"))
        {
            Debug.Log ("View/FPS Back: " + EditorPrefs.GetString("View/FPS Back"));
            Debug.Log ("View/FPS Forward: " + EditorPrefs.GetString("View/FPS Forward"));
            Debug.Log ("View/FPS Strafe Down: " + EditorPrefs.GetString("View/FPS Strafe Down"));
            Debug.Log ("View/FPS Strafe Left: " + EditorPrefs.GetString ("View/FPS Strafe Left"));
            Debug.Log ("View/FPS Strafe Right: " + EditorPrefs.GetString("View/FPS Strafe Right"));
            Debug.Log ("View/FPS Strafe Up: " + EditorPrefs.GetString("View/FPS Strafe Up"));
            Debug.Log ("Tools/Move: " + EditorPrefs.GetString(""));
            Debug.Log ("Tools/Pivot Mode: " + EditorPrefs.GetString(""));
            Debug.Log ("Tools/Pivot Rotation: " + EditorPrefs.GetString(""));
            Debug.Log ("Tools/Rect Handles: " + EditorPrefs.GetString(""));
            Debug.Log ("Tools/Rotate: " + EditorPrefs.GetString(""));
            Debug.Log ("Tools/Scale: " + EditorPrefs.GetString(""));
            Debug.Log ("Tools/View: " + EditorPrefs.GetString(""));
        }             

        if (GUILayout.Button ("Load Prefs"))
        {
            setKeyPref("Tools/Move", "Comma");
            setKeyPref("Tools/Pivot Mode", "Semicolon");
            setKeyPref("Tools/Pivot Rotation", "Q");
            setKeyPref("Tools/Rect Handles", "Y");
            setKeyPref("Tools/Rotate", "Period");
            setKeyPref("Tools/Scale", "P");
            setKeyPref("Tools/View", "Quate");

            // EditorPrefs.SetString("View/FPS Back", "View/FPS Back;O");
            setKeyPref("View/FPS Back", "O");
            setKeyPref("View/FPS Forward", "Comma");
            setKeyPref("View/FPS Strafe Down", "Quote");
            setKeyPref("View/FPS Strafe Left", "A");
            setKeyPref("View/FPS Strafe Right", "E");
            setKeyPref("View/FPS Strafe Up", "Period");
        }

        GUILayout.EndHorizontal();

        EndWindows();
    }

    void setKeyPref (string pref, string val)
    {
        EditorPrefs.SetString(pref, pref + ";" + val);
    }
}

#endif
