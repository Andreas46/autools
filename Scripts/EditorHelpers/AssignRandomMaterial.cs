﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor (typeof (AssignRandomMaterial))]
[CanEditMultipleObjects]
public class EditorAssignRandomMaterial :  Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		//SetObjOutsideFOV theScript = (SetObjOutsideFOV) target;
		GUILayout.BeginHorizontal();
		if (GUILayout.Button ("Random Material"))
		{
			//theScript.setObjOutsideFOV (theScript.m_ScaleingFactor.x, theScript.m_ScaleingFactor.y);
			foreach (AssignRandomMaterial script in targets)
			{
				script.assignRandMatToAll();
			}
		}

		if (GUILayout.Button ("Random All"))
		{
			foreach (AssignRandomMaterial script in targets)
			{
				// Get all of this type of script
				foreach (AssignRandomMaterial subScript in script.gameObject.GetComponents <AssignRandomMaterial>())
				{
					subScript.assignRandMatToAll();
				}
			}
		}

		GUILayout.EndHorizontal ();
	}
}

#endif

[System.Serializable]
public class MatMap
{
	public GameObject m_object;
	public int m_matIndex;
}

public class AssignRandomMaterial : MonoBehaviour 
{
	public string m_label;
	public List <Material> m_materialSources;
	public List <MatMap> m_targets;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void assignRandMatToAll ()
	{
//		float numSharedMats = m_targets [0].GetComponent <Renderer> ().sharedMaterials.Length;
//		for (int i = 0; i < numSharedMats;  ++i)
//		{
//			assignRandomMat (i);
//		}
		assignRandomMat ();
	}

	void assignRandomMat ()
	{
		if (m_materialSources.Count == 0)
		{
			Debug.LogWarning ("No material sources set!");
			return;
		}

		int newMatIndex = Random.Range (0, m_materialSources.Count);

		foreach  (MatMap matMap in m_targets)
		{
			Material[] materials = matMap.m_object.GetComponent <Renderer>().sharedMaterials;

#if UNITY_EDITOR
			Undo.RecordObject (matMap.m_object.GetComponent <Renderer>(), "Swap Material");
#endif
			materials [matMap.m_matIndex] = m_materialSources [newMatIndex];

			matMap.m_object.GetComponent <Renderer>().sharedMaterials = materials;
		}

	}
}





