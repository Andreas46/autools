﻿using UnityEngine;
using System.Collections;

namespace AuTools
{
    static public class ExtremePoints
    {
        public static Vector3 getLowestPtRel(this GameObject obj)
        {
            return getYExtremeRelative(obj, true);
        }

        public static Vector3 getYExtremeRelative(this GameObject obj, bool getLowestPoint)
        {
            float? extremePt = getYExtreme(obj, getLowestPoint);

            Vector3 retVec = new Vector3();

            if (extremePt.HasValue)
            {
                retVec.y = extremePt.Value - obj.transform.position.y;
            }

            return retVec;
        }

        public static Vector3 getYExtremeVec(this GameObject obj, bool getLowestPoint)
        {
            float? extremePt = getYExtreme(obj, getLowestPoint);

            Vector3 retVec = new Vector3();

            if (extremePt.HasValue)
            {
                retVec.y = extremePt.Value;
            }

            return retVec;
        }

        static float? getYExtreme(GameObject obj, bool getLowestPoint)
        {
            float yExtreme;
            float? retVal = null;

            Collider collider = obj.GetComponent<Collider>();
            Renderer renderer = obj.GetComponent<Renderer>();
            if (collider)
            {
                if (getLowestPoint)
                {
                    yExtreme = -collider.bounds.extents.y;
                }
                else
                {
                    yExtreme = collider.bounds.extents.y;  // Get highest Point.
                }

                yExtreme += collider.bounds.center.y;

                //Debug.Log ("Y: " + (obj.transform.position - collider.bounds.center).y);
                //lowestPoint.y +=collider.

                retVal = yExtreme;
            }
            else if (renderer)
            {
                if (getLowestPoint)
                {
                    yExtreme = -renderer.bounds.extents.y;
                }
                else
                {
                    yExtreme = renderer.bounds.extents.y;  // Get highest Point.
                }

                yExtreme += renderer.bounds.center.y;

                retVal = yExtreme;
            }

            float? childExtremeY = null;

            // Compair with lowest point of all children.
            foreach (Transform child in obj.transform)
            {
                childExtremeY = getYExtreme(child.gameObject, getLowestPoint);

                if (childExtremeY.HasValue)
                {
                    // If we don't yet have a lowest point
                    if (!retVal.HasValue)
                    {
                        retVal = childExtremeY;
                    }

                    // If we're getting the lowest point of the child and it is
                    // lower that our current lowest point.
                    if ((getLowestPoint && childExtremeY.Value < retVal.Value) ||
                          // Or we're getting the highest, and it is higher that the current point.
                          !getLowestPoint && childExtremeY.Value > retVal.Value)
                    {
                        retVal = childExtremeY;
                    }
                }
            }

            return retVal;
        }
    }

    static public class ObjMeasurments
    {
        //static public Vector3 getGlobalDimensions (this GameObject obj)
        //{

        //}

        static public Bounds getGlobalBounds (GameObject[] objs)
        {
            Vector3? maxPoint = null;
            Vector3? minPoint = null;

            foreach (GameObject obj in objs)
            {
                Bounds? bounds = getGlobalBoundsImpl (obj, maxPoint, minPoint);

                if (bounds != null)
                {
                    maxPoint = bounds.Value.max;
                    minPoint = bounds.Value.min;
                }
            }

            if (minPoint == null || maxPoint == null)
            {
                return new Bounds (objs[0].transform.position, Vector3.zero);
            }
            else
            {
                Vector3 size = maxPoint.Value - minPoint.Value;
                Vector3 centre = minPoint.Value + (size * 0.5f);

                //Debug.Log ("Retvec: " + size);

                return new Bounds(centre, size);
            }
        }

		static public Bounds getGlobalBounds (this GameObject obj, bool ignoreColliders = false)
        {
			Bounds? bounds = getGlobalBoundsImpl(obj, null, null, ignoreColliders);

            if (bounds != null)
            {
                return bounds.Value;
            }
            else
            {
                return new Bounds (obj.transform.position, Vector3.zero);
            }
        }

		static Bounds? getGlobalBoundsImpl (this GameObject obj, Vector3? initMaxPt = null, Vector3? initMinPt = null,
			bool ignoreColliders = false)
        {
            Collider collider = obj.GetComponent <Collider>();
            Renderer renderer = obj.GetComponent <Renderer>();

            Vector3? maxPoint = initMaxPt;
            Vector3? minPoint = initMinPt;

			if (collider && !ignoreColliders)
            {
                //Vector3 retDimensions = obj.b
                minPoint = collider.bounds.min;
                maxPoint = collider.bounds.max;
            }
            else if (renderer)
            {
                minPoint = renderer.bounds.min;
                maxPoint = renderer.bounds.max;
            }

            if (minPoint != null && initMinPt != null)
            {
                minPoint = Vector3.Min(minPoint.Value, initMinPt.Value);
                maxPoint = Vector3.Max(maxPoint.Value, initMaxPt.Value);
            }

            // Recurse into the children and compair with their dimensions.
            foreach (Transform trans in obj.transform)
            {
				Bounds? bounds = trans.gameObject.getGlobalBounds(ignoreColliders);

                if (bounds != null)
                {
                    if (maxPoint == null || minPoint == null)
                    {
                        maxPoint = bounds.Value.max;
                        minPoint = bounds.Value.min;
                    }
                    else
                    {
                        maxPoint = Vector3.Max(maxPoint.Value, bounds.Value.max);
                        minPoint = Vector3.Min(minPoint.Value, bounds.Value.min);
                    }
                }
            }

            if (maxPoint == null || minPoint == null)
            {
                return null;
            }
            else
            {
                Vector3 size = maxPoint.Value - minPoint.Value;
                Vector3 centre = minPoint.Value + (size * 0.5f);

               // Debug.Log("Max Point: " + minPoint.Value);

                return new Bounds (centre, size);
            }
        }
    }
}

