﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class AnimParam
{
	enum AnimParamType
	{
		typeBool,
		typeFloat
	}

	public string m_name;
	public int m_paramType;

	public bool m_boolVal;
	public float m_floatVal;

	public AnimParam (string name, bool boolVal)
	{
		m_name = name;

		m_paramType = (int) AnimParamType.typeBool;
		m_boolVal = boolVal;
	}

	public AnimParam (string name, float floatVal)
	{
		m_name = name;
		
		m_paramType = (int) AnimParamType.typeFloat;
		m_floatVal = floatVal;
	}

	public void setInAnimator (Animator animator)
	{
		if (m_paramType == (int) AnimParamType.typeBool)
		{
			animator.SetBool (m_name, m_boolVal);
		}
		else if (m_paramType == (int) AnimParamType.typeFloat)
		{
			animator.SetFloat (m_name, m_floatVal);
		}
	}
};

#if UNITY_EDITOR

[CustomEditor (typeof (DefaultAnimatorParams))]
[CanEditMultipleObjects]
public class DefaultAnimParams :  Editor
{
	public override void OnInspectorGUI()
	{
		//DrawDefaultInspector ();
		
		// GUILayout.BeginHorizontal ();

		Animator animator = null;

		foreach (DefaultAnimatorParams script in targets)
		{
			if (!animator)
			{
				animator = script.GetComponent <Animator>();
			}
			else if (animator.avatar != script.GetComponent <Animator>().avatar)
			{
				GUIStyle style = new GUIStyle();
				style.normal.textColor = Color.red;

				EditorGUILayout.LabelField ("Editing multiple different animators is not supported.",  style);
				return;
			}
		}

		EditorGUILayout.BeginHorizontal ();

		{
			bool? initalVal = null;
			
			foreach (DefaultAnimatorParams script in targets)
			{
				if (initalVal == null)
				{
					initalVal = script.m_randomStartFrame;
				}
				else
				{
					if (initalVal.Value != script.m_randomStartFrame)
					{
						EditorGUI.showMixedValue = true;
					}
				}
			}
			
			EditorGUI.BeginChangeCheck();
			
			bool toggleVal = EditorGUILayout.Toggle ("Random Start Frame", initalVal.Value);
			
			if (EditorGUI.EndChangeCheck() == true)
			{
				foreach (DefaultAnimatorParams script in targets)
				{
					script.m_randomStartFrame = toggleVal;
				}
			}
			
			EditorGUI.showMixedValue = false;
		}

		{
			float? initalVal = null;
			
			foreach (DefaultAnimatorParams script in targets)
			{
				if (initalVal == null)
				{
					initalVal = script.m_clipMaxPercent;
				}
				else
				{
					if (initalVal.Value != script.m_clipMaxPercent)
					{
						EditorGUI.showMixedValue = true;
					}
				}
			}
			
			EditorGUI.BeginChangeCheck();
			
			//float floatVal = EditorGUILayout.FloatField (param.name, initalVal.Value);
			float floatVal = EditorGUILayout.Slider (initalVal.Value, 0.0f, 1.0f);
			
			if (EditorGUI.EndChangeCheck() == true)
			{
				foreach (DefaultAnimatorParams script in targets)
				{
					script.m_clipMaxPercent = floatVal;
				}
			}
		}

		EditorGUILayout.EndHorizontal ();

		// Horizontal rule.
		GUILayout.Box("", new GUILayoutOption[]{GUILayout.ExpandWidth(true), GUILayout.Height(1)});
		
		for (int i = 0; i < animator.parameters.Length; ++i)
		{
			AnimatorControllerParameter param = animator.parameters [i];

			if (param.type == AnimatorControllerParameterType.Bool)
			{
				bool? initalVal = null;

				foreach (DefaultAnimatorParams script in targets)
				{
					if (initalVal == null)
					{
						initalVal = script.getBool (param.name);
					}
					else
					{
						if (initalVal.Value != script.getBool (param.name))
						{
							EditorGUI.showMixedValue = true;
						}
					}
				}

				EditorGUI.BeginChangeCheck();

				bool toggleVal = EditorGUILayout.Toggle (param.name, initalVal.Value);

				if (EditorGUI.EndChangeCheck() == true)
				{
					foreach (DefaultAnimatorParams script in targets)
					{
						script.setInitParamState (new AnimParam (param.name, toggleVal));
					}
				}

				EditorGUI.showMixedValue = false;
			}
			else if (param.type == AnimatorControllerParameterType.Float)
			{
				float? initalVal = null;
				
				foreach (DefaultAnimatorParams script in targets)
				{
					if (initalVal == null)
					{
						initalVal = script.getFloat (param.name);
					}
					else
					{
						if (initalVal.Value != script.getFloat (param.name))
						{
							EditorGUI.showMixedValue = true;
						}
					}
				}

				EditorGUI.BeginChangeCheck();

				float floatVal = EditorGUILayout.FloatField (param.name, initalVal.Value);

				if (EditorGUI.EndChangeCheck() == true)
				{
					foreach (DefaultAnimatorParams script in targets)
					{
						script.setInitParamState (new AnimParam (param.name, floatVal));
					}
				}
			}
			//}
		}

		foreach (DefaultAnimatorParams script in targets)
		{
			// Reserialise modified script parameters.
			EditorUtility.SetDirty (script);
		}

		int defaultStateHash = 0;

		foreach (DefaultAnimatorParams script in targets)
		{
			if (script.m_startStateHash == 0)
			{
				// Set name of starting state.
				script.m_startStateHash = animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
			}

			if (defaultStateHash == 0)
			{
				defaultStateHash = script.m_startStateHash;
			}

			if (defaultStateHash != script.m_startStateHash)
			{
				EditorGUI.showMixedValue = true;
				break;
			}
		}

		List <string> stateNames = new List <string> ();
		int selectedIndex = 1;

		// Generate a list of names of states.
		UnityEditor.Animations.AnimatorController animCtl = animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
		foreach (UnityEditor.Animations.ChildAnimatorState aState in animCtl.layers [0].stateMachine.states)
		{
			if (defaultStateHash != 0 && defaultStateHash == aState.state.nameHash)
			{
				selectedIndex = stateNames.Count;
			}
			else if (defaultStateHash == 0 && animator.GetCurrentAnimatorStateInfo(0).IsName (aState.state.name))
			{
				selectedIndex = stateNames.Count;
			}

			//Debug.Log ("Added name:" + aState.state.name);
			stateNames.Add (aState.state.name);
		}

		EditorGUI.BeginChangeCheck();

		selectedIndex = EditorGUILayout.Popup (selectedIndex, stateNames.ToArray());

		EditorGUI.showMixedValue = false;

		if (EditorGUI.EndChangeCheck() == true)
		{
			foreach (DefaultAnimatorParams script in targets)
			{
				script.GetComponent <Animator>().Play (stateNames [selectedIndex]);
				script.m_startStateHash = Animator.StringToHash (stateNames [selectedIndex]);
			}
		}
	}

//	void mixedParam <retT, objT> ()
//	{
//		T? initalVal = null;
//
//		foreach (objT script in targets)
//		{
//			if (initalVal == null)
//			{
//				initalVal = script.getBool (param.name);
//			}
//			else
//			{
//				if (initalVal.Value != script.getBool (param.name))
//				{
//					EditorGUI.showMixedValue = true;
//				}
//			}
//		}
//
//		EditorGUI.BeginChangeCheck();
//
//		bool toggleVal = EditorGUILayout.Toggle (param.name, initalVal.Value);
//
//		if (EditorGUI.EndChangeCheck() == true)
//		{
//			foreach (DefaultAnimatorParams script in targets)
//			{
//				script.setInitParamState (new AnimParam (param.name, toggleVal));
//			}
//		}
//
//		EditorGUI.showMixedValue = false;
//	}
}

#endif

[RequireComponent (typeof (Animator))]
public class DefaultAnimatorParams : MonoBehaviour
{

	//List <AnimParam> m_paramerters;
	[SerializeField] [HideInInspector]
	List <AnimParam> m_parameters = new List <AnimParam> ();

	public int m_startStateHash;

	public bool m_randomStartFrame = true;
	public float m_clipMaxPercent = 1.0f;

	float m_originalSpeed = 1.0f;
	
	void Start ()
	{
		Animator animator = GetComponent <Animator> ();

		// Set animator parameters
		foreach (AnimParam param in m_parameters)
		{
			param.setInAnimator (animator);
		}

		if (m_randomStartFrame && m_clipMaxPercent > 0.0f)
		{
			// Evaluate the animation start frames after the animator has had a few frames to get started.
			//Invoke ("evaluateRandomFrameStart", 1.0f);
			animator.Play (m_startStateHash);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void evaluateRandomFrameStart()
	{
		Animator animator = GetComponent <Animator> ();

		//float longestClipTime = 0.0f;

		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo (0);

		//float clipLength = stateInfo.length;
		float targetTime = Random.Range (0.0f, m_clipMaxPercent) * stateInfo.length;

	//	Debug.Log ("Target time: " + targetTime);
		//Debug.Log ("Length: " + stateInfo.length);

		m_originalSpeed = animator.speed;

		// Speed up to 10x.
		animator.speed = 10.0f;

		// Stop the animation when we've reached approximately the target location.
		Invoke ("resetSpeed", targetTime / animator.speed);
	}

	void resetSpeed()
	{
		GetComponent <Animator>().speed = m_originalSpeed;
	}

	public void setInitParamState (AnimParam param)
	{
		//m_parameters [param.m_name] = param;
		bool replacedExisting = false;

		// Do we already have a param with this name registered?
		for (int i = 0; i < m_parameters.Count; ++i)
		{
			if (m_parameters [i].m_name == param.m_name)
			{
				// Replace the existing.
				m_parameters [i] = param;
				replacedExisting = true;
				break;
			}
		}

		if (!replacedExisting)
		{
			m_parameters.Add (param);
		}

#if UNITY_EDITOR
		EditorUtility.SetDirty (this);
#endif
	}

	public bool getBool (string id)
	{
		if (isParamRegistered (id))
		{
			return getParam (id).m_boolVal;
		}
		else
		{
			return GetComponent <Animator>().GetBool (id);
		}
	}

	public float getFloat (string id)
	{
		if (isParamRegistered (id))
		{
			return getParam (id).m_floatVal;
		}
		else
		{
			return GetComponent <Animator>().GetFloat (id);
		}
	}

	AnimParam getParam (string id)
	{
		foreach (AnimParam param in m_parameters)
		{
			if (param.m_name == id)
			{
				return param;
			}
		}

		// Param not found.
		return null;
	}

	bool isParamRegistered (string id)
	{
		foreach (AnimParam param in m_parameters)
		{
			if (param.m_name == id)
			{
				return true;
			}
		}

		return false;
	}
}
