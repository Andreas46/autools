﻿// Only compile when using the editor.
#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections;

using AuTools;

public class FitColliders: EditorWindow
{
	[MenuItem ("Window/Fit Colliders")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		FitColliders window = (FitColliders) EditorWindow.GetWindow (typeof (FitColliders));
		//window.titleContent = new GUIContent ("unitometer");
		window.Show();
	}

	FitColliders ()
	{
		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}

	void OnSceneGUI(SceneView sceneView)
	{
		Repaint();
	}

	void OnGUI()
	{
		BeginWindows();

		//m_ignoreTriggers = GUILayout.Toggle (m_ignoreTriggers, "Ignore Triggers");

		GUILayout.BeginHorizontal();

		if (GUILayout.Button("FitCollider"))
		{
			if (Selection.activeTransform)
			{
				//				Undo.RecordObject (Selection.activeTransform, "Move Object");

				fitCollider ();
			}
		}

		GUILayout.EndHorizontal ();

		EndWindows();
	}

	void fitCollider ()
	{
		Transform trans = Selection.activeTransform;

		Bounds bounds = trans.gameObject.getGlobalBounds (true);
		Vector3 inverseScale = trans.localScale;

		if (inverseScale.x != 0)
		{
			inverseScale.x = 1 / inverseScale.x;
		}
		if (inverseScale.y != 0)
		{
			inverseScale.y = 1 / inverseScale.y;
		}
		if (inverseScale.z != 0)
		{
			inverseScale.z = 1 / inverseScale.z;
		}

		Vector3 extents = bounds.extents;
		extents = Vector3.Scale (extents, inverseScale);
		Debug.Log ("Extents: " + extents);

		Collider collider = Selection.activeTransform.GetComponent <Collider> ();

		if (collider == null)
		{
			return;
		}

		if (collider is CapsuleCollider)
		{
			CapsuleCollider capCol = collider as CapsuleCollider;

			capCol.radius = Mathf.Max (extents.x, extents.z);
			capCol.height = extents.y * 2;

			Vector3 offset = bounds.center - trans.position;

			capCol.center = Vector3.Scale (offset, inverseScale);
		}
	}
}

#endif
