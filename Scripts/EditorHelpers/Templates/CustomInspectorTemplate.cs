﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

// Replace "CustomInspectorTemplate"
// Replace "BeingEdited" with the class to be edited.

[CustomEditor (typeof (BeingEdited))]
[CanEditMultipleObjects]
public class CamManagerCustomInspector : Editor
{
	public override void OnInspectorGUI()
	{
		// This Draws what we normally see in the inspector.
		DrawDefaultInspector();

		// Horizontal rule.
		GUILayout.Box("", new GUILayoutOption[]{GUILayout.ExpandWidth(true), GUILayout.Height(1)});

		// Do not render custom inspector, unless it is playing.
		if (!Application.isPlaying)
		{
			return;
		}

		GUILayout.BeginHorizontal();

		if (GUILayout.Button ("Random Material"))
		{
			foreach (BeingEdited script in targets)
			{
				script.aFunc();
			}
		}

		GUILayout.EndHorizontal();
	}

}

#endif

// Ignore This.
public class BeingEdited: MonoBehaviour
{
	public void aFunc ()
	{
		
	}
}

