﻿
#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

using AuTools;

public class NestedPrefabWindow: EditorWindow
{
	List <NestedPrefab> m_selectedNestedPrefabs = new List <NestedPrefab>();

	[MenuItem ("Window/Nested Prefab")]
	//[MenuItem ("Window/My Window")]
	static void Init ()
	{
		// Get existing open window or if none, make a new one:
		NestedPrefabWindow window = (NestedPrefabWindow) EditorWindow.GetWindow (typeof (NestedPrefabWindow));
		//window.title = "Nested Prefabs";
		window.titleContent = new GUIContent ("Nested Prefabs");
		window.minSize = new Vector2 (10.0f,10.0f);
		window.Show();
	}

	NestedPrefabWindow()
	{
		//Debug.Log ("Initialising Move Obj Floor");
		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}

	void OnSceneGUI (SceneView sceneView)
	{

	}

	void OnGUI()
	{
		BeginWindows();
		
		GUILayout.BeginHorizontal();
		
		if (GUILayout.Button("Instance All"))
		{
			instanceAll ();
		}
		
		if (GUILayout.Button("Apply Selected"))
		{
			applySelected();
		}

		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal();

		if (GUILayout.Button("Remove All"))
		{
			removeAll();
		}

		if (GUILayout.Button("Revert All"))
		{
			revertAll();
		}

		GUILayout.EndHorizontal ();
		EndWindows();
	}

	void findNestedGORecusive (Transform searchIn)
	{
		NestedPrefab nestedPrefab = searchIn.GetComponent <NestedPrefab> ();

		if (nestedPrefab)
		{
			m_selectedNestedPrefabs.Add (nestedPrefab);
		}
		
		// Search the children.
		foreach (Transform trans in searchIn)
		{
			findNestedGORecusive (trans);
		}
	}

	void updateSelectedNestedPrefabs (bool stopAtFirstHit = true)
	{
		m_selectedNestedPrefabs.Clear ();

		foreach (GameObject obj in Selection.gameObjects)
		{
			int foundNestedPrefabs = m_selectedNestedPrefabs.Count;

			//findNestedGORecusive (obj.transform);
            List<NestedPrefab> foundNFabs = obj.transform.getComponentsRecursive<NestedPrefab>(stopAtFirstHit);

			m_selectedNestedPrefabs.AddRange (foundNFabs);

			// If we didn't find any new nested prefabs in this selection.
			if (m_selectedNestedPrefabs.Count <= foundNestedPrefabs)
			{
				Transform trans = obj.transform;

				// Search down in the hirerachy for a parent with a nested prefab.
				while (trans != null)
				{
					NestedPrefab nestedPrefab = trans.GetComponent <NestedPrefab>();
					if (nestedPrefab)
					{
						m_selectedNestedPrefabs.Add (nestedPrefab);
						break;
					}

					trans = trans.parent;
				}
			}

			// If we still haven't found any nested prefabs.
			if (m_selectedNestedPrefabs.Count <= foundNestedPrefabs)
			{
				// Search backwards, until we find a common prefab instance, and the search recursively again.
				GameObject parentPrefab = PrefabUtility.FindRootGameObjectWithSameParentPrefab (obj);

				findNestedGORecusive (parentPrefab.transform);
			}
		}

//		// If we didn't find any nested prefab's, search backwards until
//		// we no longer can, or we find one.
//		if (m_selectedNestedPrefabs.Count == 0)
//		{
//			Transform trans = 
//		}
	}

	void applyAll ()
	{
		updateSelectedNestedPrefabs ();

		foreach (NestedPrefab nFab in m_selectedNestedPrefabs)
		{
			nFab.applyToPrefab();
		}
	}

	void applySelected()
	{
		updateSelectedNestedPrefabs (true);

		// Apply the first found nfab.
		if (m_selectedNestedPrefabs.Count > 0)
		{
			m_selectedNestedPrefabs [0].applyToPrefab();
		}
	}

	void instanceAll ()
	{
		updateSelectedNestedPrefabs (true);

		foreach (NestedPrefab nFab in m_selectedNestedPrefabs)
		{
			nFab.instancePrefab (true);
		}
	}

	void removeAll ()
	{
		updateSelectedNestedPrefabs ();
		
		foreach (NestedPrefab nFab in m_selectedNestedPrefabs)
		{
			if (nFab == null)
			{
				continue;
			}

			//nFab.removePrefabInstance();
			NestedPrefab.removePrefabInstanceRecursive (nFab);
		}

	}

	void revertAll ()
	{
		updateSelectedNestedPrefabs ();
		
		foreach (NestedPrefab nFab in m_selectedNestedPrefabs)
		{
			nFab.revertToParent();
		}
	}	
}

#endif
