﻿
#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

using AuTools;


[System.Serializable]
public class SerialisablePropertyMod
{
	public Object objectReference;
	public string propertyPath; 
	public string value; 
	public Object target;

	public SerialisablePropertyMod ()
	{
	}

	public SerialisablePropertyMod (PropertyModification mod)
	{
		objectReference = mod.objectReference;
		propertyPath = mod.propertyPath; 
		value = mod.value; 
		target = mod.target;
	}

	public PropertyModification asPropMod ()
	{
		PropertyModification retMod = new PropertyModification();

		retMod.objectReference = objectReference;
		retMod.propertyPath = propertyPath;
		retMod.value = value;
		retMod.target = target;

		return retMod;
	}

	public static PropertyModification[] convertArray (List <SerialisablePropertyMod> mods)
	{
		PropertyModification[] retMods = new PropertyModification [mods.Count];

		for (int i = 0; i < mods.Count; ++i)
		{
			retMods [i] = mods [i].asPropMod();
		}

		return retMods;
	}

	public static List <SerialisablePropertyMod> convertArray (PropertyModification[] mods)
	{
		List <SerialisablePropertyMod> retList = new List <SerialisablePropertyMod> ();

		foreach (PropertyModification mod in mods)
		{
			retList.Add (new SerialisablePropertyMod (mod));
		}

		return retList;
	}
}

[System.Serializable]
public class GameObjPair
{
	public GameObject m_targetObj;
	public GameObject m_attachPointOnPrefab;

	public Vector3 m_position;
	public Quaternion m_rotation;

	public GameObjPair (GameObject targetObj, GameObject attachPointOnPrefab, Vector3 pos, Quaternion rot)
	{
		m_targetObj = targetObj;
		m_attachPointOnPrefab = attachPointOnPrefab;

		m_position = pos;
		m_rotation = rot;
	}
}


[CustomEditor (typeof (NestedPrefab))]
[CanEditMultipleObjects]
public class EditorNestedPrefab :  Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		GUILayout.BeginHorizontal();

		if (GUILayout.Button ("Instantiate", GUILayout.ExpandWidth (false)))
		{
			foreach (NestedPrefab script in targets)
			{
				script.instancePrefab();
			}
		}

		if (GUILayout.Button ("Remove", GUILayout.ExpandWidth (false)))
		{
			foreach (NestedPrefab script in targets)
			{
				//script.removePrefabInstance ();
				script.removePrefabInstanceMulti();
			}
		}

		if (GUILayout.Button ("Apply", GUILayout.ExpandWidth (false)))
		{
			foreach (NestedPrefab script in targets)
			{
				script.applyToPrefab();
			}
		}

		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal ();

		if (GUILayout.Button ("Revert To Parent", GUILayout.ExpandWidth (false)))
		{
			foreach (NestedPrefab script in targets)
			{
				//script.debugPrintModifiedProperies ();
				script.revertToParent();
			}
		}

		if (GUILayout.Button ("Revert To Prefab", GUILayout.ExpandWidth (false)))
		{
			foreach (NestedPrefab script in targets)
			{
				//script.debugPrintModifiedProperies ();
				script.revertToPrefab();
			}
		}

		GUILayout.EndHorizontal ();

		GUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Flush", GUILayout.ExpandWidth (false)))
		{
			NestedPrefab.flushNestedPrefabInstances();
		}

		if (GUILayout.Button ("saveRefs", GUILayout.ExpandWidth (false)))
		{
			foreach (NestedPrefab script in targets)
			{
				//script.debugPrintModifiedProperies ();
				script.saveParentInstObjectReferences();
			}
		}

		if (GUILayout.Button ("RestoreRefs", GUILayout.ExpandWidth (false)))
		{
			foreach (NestedPrefab script in targets)
			{
				//script.debugPrintModifiedProperies ();
				script.restoreParentInstObjectReferences();
			}
		}

		GUILayout.EndHorizontal ();
	}
}

[System.Serializable]
[ExecuteInEditMode]
public class NestedPrefab : MonoBehaviour
{
	public GameObject m_prefab;

	[SerializeField] //[HideInInspector]
	List <SerialisablePropertyMod>  m_parentPropMods;

	[SerializeField] //[HideInInspector]
	List <SerialisablePropertyMod>  m_LocalPropMods;

	[SerializeField] //[HideInInspector]
	List <SerialisablePropertyMod>  m_parentNFabReferences = new List <SerialisablePropertyMod>();
	
	[SerializeField] //[HideInInspector]
	GameObject m_prefabInst;

	[SerializeField] //[HideInInspector]
	List <GameObjPair> m_detachedObjTrees = new List <GameObjPair>();

	// We need to track all instances of objects using this script for propergation of
	// changes.  Key = "prefab", Value = list of "NestedPrefab" instances.
	static Dictionary <GameObject, Dictionary <int, NestedPrefab> > s_AllNestedPrefabs = new Dictionary <GameObject, Dictionary <int, NestedPrefab> > ();

	// We also need another one to track all the nestedPrefabs, all the time.
	static HashSet <NestedPrefab> s_AllNFabTracker = new HashSet <NestedPrefab>();
	
	GameObject m_parentPrefabInst = null;
	GameObject m_parentPrefab = null;

	GameObject m_allFabsKey;
	int m_lastInstanceId;

	NestedPrefab ()
	{
		s_AllNFabTracker.Add (this);
	}

	~NestedPrefab ()
	{
		s_AllNFabTracker.Remove (this);
	}
	
	void Start()
	{
		updateS_AllPrefabs ();
	}

	void OnDestroy()
	{
		//print ("Destroying NestedPrefab");

		unregisterInstanceId ();
	}

	void unregisterInstanceId ()
	{
		if (m_allFabsKey != null &&
		    s_AllNestedPrefabs.ContainsKey (m_allFabsKey) &&
		    s_AllNestedPrefabs [m_allFabsKey].ContainsKey (m_lastInstanceId))
		{
			s_AllNestedPrefabs [m_allFabsKey].Remove (m_lastInstanceId);
			//print ("NestedPrefab destroyed");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		sanityCheckInstanceId ();
		checkForParentUpdate ();

		if (this == null)
		{
			Debug.LogError ("Fwah!??");
		}
	}

	void updateS_AllPrefabs()
	{
		GameObject prefab = getParentPrefab ();
		
		if (prefab)
		{
			if (m_allFabsKey)
			{
				// Remove previous entry.
				unregisterInstanceId();
			}
			
			// If we're not tracking this prefab, change this now.
			if (!s_AllNestedPrefabs.ContainsKey (prefab))
			{
				s_AllNestedPrefabs.Add (prefab, new Dictionary <int, NestedPrefab>());
			}
			
			s_AllNestedPrefabs [prefab].Add (GetInstanceID(), this);
			
			m_allFabsKey = prefab;
			m_lastInstanceId = GetInstanceID();
		}
	}

	void checkForParentUpdate()
	{
		if (getParentPrefab() == null)
		{
			// Still not part of a prefab.  Skip update.
			Debug.LogWarning ("Skipping null parent prefab (update).");
			return;
		}

		if (m_allFabsKey == null ||
		    m_allFabsKey != getParentPrefab() ||
		    s_AllNestedPrefabs.ContainsKey (getParentPrefab ()) == false ||
		    s_AllNestedPrefabs [getParentPrefab()].ContainsKey (GetInstanceID()) == false)
		{
			Debug.LogWarning ("Parent Changed!");
			updateS_AllPrefabs();
			updateParentPrefabInfo();
		}
	}

	void sanityCheckInstanceId()
	{
		// Check to see if the objects unique id has changed (to indicate a duplication of objects, etc)
		if (GetInstanceID() != m_lastInstanceId)
		{
			Debug.Log ("Instance Id changed!");

			unregisterInstanceId();
			updateS_AllPrefabs();
			updateParentPrefabInfo();
		}
	}

	void sanitiseSAllNestedPrefabs ()
	{
		foreach (KeyValuePair <int, NestedPrefab> nFabPair in s_AllNestedPrefabs [m_allFabsKey])
		{
			if (nFabPair.Value == null)
			{
				Debug.LogWarning ("Bad registered nested prefab!???" );
				s_AllNestedPrefabs [m_allFabsKey].Remove (nFabPair.Key);
				
				sanitiseSAllNestedPrefabs();
				return;
			}
		}
	}

	// Remove references to instances of NestedPrefab, that have been destroyed by the unity engine.
	void sanitiseSAllNFabTracker ()
	{
		foreach (NestedPrefab nFab in s_AllNFabTracker)
		{
			if (nFab == null)
			{
				Debug.LogWarning ("Bad nested prefab being tracked!???" );
				s_AllNFabTracker.Remove (nFab);
				
				sanitiseSAllNFabTracker();
				return;
			}
		}
	}

	public void onParentPrefabUpdate ()
	{
		// Skip update if we're calling this on the prefab it's self.
		if (PrefabUtility.GetPrefabType (this.gameObject) == PrefabType.Prefab)
		{
			return;
		}

		sanityCheckInstanceId ();
		checkForParentUpdate ();
	}
	
	void updateParentPrefabInfo ()
	{
		// If we're not attached to anything, this function has no purpose.
		if (transform.parent == null)
		{
			Debug.LogWarning ("Nested prefab has no parent!");

			m_parentPrefabInst = null;
			m_parentPrefab = null;
			return;
		}

		GameObject instanceRoot = PrefabUtility.FindRootGameObjectWithSameParentPrefab (transform.parent.gameObject);
		GameObject prefab = (GameObject) PrefabUtility.GetPrefabParent (instanceRoot);

		if (instanceRoot == null)
		{
			Debug.LogWarning ("Didn't find prefab instance root!");
			return;
		}
		
		if (PrefabUtility.GetPrefabType (instanceRoot) != PrefabType.DisconnectedPrefabInstance && 
		    PrefabUtility.GetPrefabType (instanceRoot) != PrefabType.PrefabInstance)
		{
			Debug.LogWarning ("Prefab instance root is of incorrect type!? " + PrefabUtility.GetPrefabType (instanceRoot));
			return;
		}
		
		if (prefab == null)
		{
			Debug.LogWarning ("Didn't find prefab!");
			return;
		}
		
		if (PrefabUtility.GetPrefabType (prefab) != PrefabType.Prefab)
		{
			Debug.LogWarning ("prefab not a prefab!?");
			return;
		}

		if (PrefabUtility.GetPrefabType (instanceRoot) == PrefabType.DisconnectedPrefabInstance)
		{
			// Parent prefab instance has become disconnected.  This means we can not determin
			// m_parentPrefabInst, but we can be sure of the prefab.
			//Debug.LogWarning ("Prefab has become disconnected!");
		//	return;
		}

		m_parentPrefabInst = instanceRoot;
		m_parentPrefab = prefab;
	}

	GameObject getParentPrefabInst ()
	{
		updateParentPrefabInfo ();

		return m_parentPrefabInst;
	}

	GameObject getParentPrefab()
	{
		updateParentPrefabInfo ();

		return m_parentPrefab;
	}

	bool isGameObjInHierarchy (GameObject searchGoal, Transform searchIn)
	{
		// If we've found the object we're searching for.
		if (searchGoal == searchIn.gameObject)
		{
			return true;
		}

		// Search the children
		foreach (Transform trans in searchIn)
		{
			if (isGameObjInHierarchy (searchGoal, trans))
			{
				return true;
			}
		}
		
		// The target was not found.
		return false;
	}

	bool isObjInHierarchy (Object searchGoal, Transform searchIn)
	{
		// Seach components for this object.
		foreach (Component component in searchIn.GetComponents<Component>())
		{
			if  (component == searchGoal)
			{
				return true;
			}
		}

		// Search the children.
		foreach (Transform trans in searchIn)
		{
			if (isObjInHierarchy (searchGoal, trans))
			{
				return true;
			}
		}

		// The target was not found.
		return false;
	}

	List <SerialisablePropertyMod> findObjRefsInHierarchy (
		Object objToFind,
		Transform searchIn,
		bool stopAtNfab = false,
		List <SerialisablePropertyMod> outList = null)
	{
		if (outList == null)
		{
			outList = new List <SerialisablePropertyMod>();
		}

		List <Object> searchObjs = new List <Object> ();

		// Check the game object.
		//searchObjs.Add (searchIn.gameObject);

		// Check attached components
		searchObjs.AddRange (searchIn.GetComponents<Component>());

		// Seach components for references to this object.
		foreach (Object obj in searchObjs)
		{
			if (obj.getTypeName() == "UnityEngine.Transform")
			{
				//Debug.Log ("Skipping transform");
				continue;
			}

			if (stopAtNfab && obj.getTypeName() == "NestedPrefab")
			{
				Debug.Log ("Skipping nfab");
				continue;
			}

			SerializedObject serialisedObj = new SerializedObject (obj);
			SerializedProperty objProperty = serialisedObj.GetIterator();

			while (objProperty.NextVisible (true))
			{

				if (objProperty.propertyType == SerializedPropertyType.ObjectReference &&
				    objProperty.objectReferenceValue == objToFind)
				{
					SerialisablePropertyMod mod = new SerialisablePropertyMod();

					if (PrefabUtility.GetPrefabType (obj) == PrefabType.PrefabInstance ||
					    PrefabUtility.GetPrefabType (obj) == PrefabType.DisconnectedPrefabInstance)
					//if (objProperty.isInstantiatedPrefab)
					{
						// Convert objects to their assosiated prefab objects.
						//mod.target = PrefabUtility.GetPrefabParent (obj);
						mod.objectReference = PrefabUtility.GetPrefabParent (objToFind);
					}

					// Unity automagically converts even this value between it's prefab/instance object as appropriate.
					mod.target = obj;

					mod.propertyPath = objProperty.propertyPath;

					outList.Add (mod);
				}
			}
		}
		
		// Search the children.
		foreach (Transform trans in searchIn)
		{
			if (stopAtNfab && trans.GetComponent <NestedPrefab>() != null)
			{
				// Do not decent into nested prefab.
				continue;
			}

			findObjRefsInHierarchy (objToFind, trans, stopAtNfab, outList);
		}
		
		// The target was not found.
		return outList;
	}

	Object findInstanceOfPrefabObjIn (GameObject searchIn, object prefabObj)
	{
		object instObj = PrefabUtility.GetPrefabParent (searchIn);

		if (instObj == null)
		{
			Debug.LogWarning ("Can't find prefab parent for searchin obj! " + searchIn);
			return null;
		}

		if (instObj == prefabObj)
		{
			return searchIn;
		}

		// Search components
		foreach (Component comp in searchIn.GetComponents <Component>())
		{
			instObj = PrefabUtility.GetPrefabParent (comp);

			if (prefabObj == instObj)
			{
				return comp;
			}
		}

		// Search the children.
		foreach (Transform trans in searchIn.transform)
		{
			Object obj = findInstanceOfPrefabObjIn (trans.gameObject, prefabObj);
			if (obj)
			{
				return obj;
			}
		}

		return null;
	}

	public static void flushNestedPrefabInstances ()
	{
		s_AllNestedPrefabs.Clear ();
	}

	public void instancePrefab (bool recursive = false)
	{
			instancePrefab (m_prefab, recursive);
	}

    //static public List <T> getComponentsRecursive <T> (Transform searchTrans, bool stopAtFirstHit = false, List <T> outList = null)
    //{
    //    if (outList == null)
    //    {
    //        outList = new List <T> ();
    //    }

    //    T component = searchTrans.GetComponent <T> ();

    //    if (component != null)
    //    {
    //        outList.Add (component);
    //        if (stopAtFirstHit)
    //        {
    //            return outList;
    //        }
    //    }

    //    // Recurse deeper into the hierachy.
    //    foreach (Transform trans in searchTrans)
    //    {
    //        getComponentsRecursive <T> (trans, stopAtFirstHit, outList);
    //    }

    //    return outList;
    //}

	static List <Object> getAllObjectsRecursive (Transform searchTrans, List <Object> outList = null)
	{
		if (outList == null)
		{
			outList = new List <Object> ();
		}

		// Add gameobject.
		outList.Add (searchTrans.gameObject);
		
		Object[] components = searchTrans.GetComponents <Component> ();
		
		if (components != null)
		{
			outList.AddRange (components);
		}
		
		// Recurse deeper into the hierachy.
		foreach (Transform trans in searchTrans)
		{
			getAllObjectsRecursive (trans, outList);
		}
		
		return outList;
	}
	
	static List <List <NestedPrefab> > getNFabsInLayers (GameObject searchIn,
	                                              List <List <NestedPrefab> > inList = null, int layer = 0)
	{
		if (inList == null)
		{
			 inList = new List <List <NestedPrefab> >();
		}

		// Retrived nestedprefabs.
        List<NestedPrefab> prefabsInLayer = searchIn.transform.getComponentsRecursive<NestedPrefab> (true);

		if (prefabsInLayer.Count > 0)
		{

			// Search all of these for deeper layers of nested prefabs.
			foreach (NestedPrefab nFab in prefabsInLayer)
			{
				if (nFab == null)
				{
					Debug.Log ("Skipping null");
					continue;
				}

				if (nFab.gameObject == searchIn)
				{
				//	Debug.Log ("Skipping this");
					continue;
				}

				// Create a new layer if necessary.
				if (inList.Count <= layer)
				{
					inList.Add (new List <NestedPrefab> ());
				}

				inList [layer].Add (nFab);

				// Search in the nested prefab's children for deeper layers.
				foreach (Transform trans in nFab.transform)
				{
					getNFabsInLayers (trans.gameObject, inList, layer + 1);
				}
			}
		}

		return inList;
	}

	void forceRecordAllPropertyChanges ()
	{
		foreach (Object obj in getAllObjectsRecursive(getParentPrefabInst().transform))
		{
			PrefabUtility.RecordPrefabInstancePropertyModifications (obj);
			EditorUtility.SetDirty (obj);
		}
	}

	static public void removePrefabInstanceRecursive (NestedPrefab fab, bool deferReconnection = false)
	{
		GameObject instanceRoot = fab.getParentPrefabInst ();
		
		if (instanceRoot == null)
		{
			// We're not connected to a parent prefab (yet?).
			//removePrefabInstance (true);
			return;
		}

		List <List <NestedPrefab> > nfabs = getNFabsInLayers (instanceRoot);

		for (int i = nfabs.Count - 1; i >= 0 ; --i)
		{
			Debug.Log ("Removing layer: " + i);

			foreach (NestedPrefab nFab in nfabs [i])
			{
				if (nFab == null)
				{
					Debug.Log ("wha!?");
					continue;
				}

				nFab.removePrefabInstance (true);
			}
		}

		// Reconnect the parent instance.
		if (!deferReconnection)
		{
			// With all the moving around of objects, the parent prefab instance
			// is requently disconnected from it's parent prefab.  WIth the instance
			// now saved, and destroyed, it is safe to attempt a reconnect.
			PrefabUtility.ReconnectToLastPrefab (instanceRoot);
		}
	}

	// If we have more than one instanc of nested prefab attached to the
	// parent prefab instance, we may not trigger a reconnection to it
	// untill all of the sibling nested prefabs have also been put into the
	// "removed" state.
	public void removePrefabInstanceMulti (bool deferReconnection = false)
	{
		List <NestedPrefab> siblingNestFabInstances;

		GameObject instanceRoot = getParentPrefabInst ();

		if (instanceRoot == null)
		{
			// We're not connected to a parent prefab (yet?).
			removePrefabInstance (true);
			return;
		}

        siblingNestFabInstances = instanceRoot.transform.getComponentsRecursive<NestedPrefab>(true);

		foreach (NestedPrefab nFab in siblingNestFabInstances)
		{
			nFab.removePrefabInstance (true);
		}

		// Reconnect the parent instance.
		if (!deferReconnection)
		{
			// With all the moving around of objects, the parent prefab instance
			// is requently disconnected from it's parent prefab.  WIth the instance
			// now saved, and destroyed, it is safe to attempt a reconnect.
			PrefabUtility.ReconnectToLastPrefab (instanceRoot);
		}
	}

	public void removePrefabInstance (bool deferReconnection = false)
	{
		if (m_prefabInst)
		{
			PropertyModification[] mods;
			//PrefabUtility.RecordPrefabInstancePropertyModifications (this);

			if (!m_prefabInst)
			{
				Debug.LogError ("inst gone!?");
				return;
			}

			PrefabUtility.RecordPrefabInstancePropertyModifications (this);

			mods = PrefabUtility.GetPropertyModifications (m_prefabInst);
			if (mods != null) 
			{
				m_LocalPropMods = SerialisablePropertyMod.convertArray (mods);
				//EditorUtility.SetDirty (this);
			}

			// Save any new objects that were attached.
			saveNewAttachedGameObjects();

			debugAttachedGos ();

			saveParentInstObjectReferences();

			EditorUtility.SetDirty (this);

			debugAttachedGos();

			// Force prefab system to record modified properties NOW.
			PrefabUtility.RecordPrefabInstancePropertyModifications (this);

			debugAttachedGos();

			// Delete first object.
			GameObject.DestroyImmediate (m_prefabInst);

			debugAttachedGos();

			if (!deferReconnection)
			{
				// With all the moving around of objects, the parent prefab instance
				// is frequently disconnected from it's parent prefab.  WIth the instance
				// now saved, and destroyed, it is safe to attempt a reconnect.
				PrefabUtility.ReconnectToLastPrefab (gameObject);
			}
		}
	}
	
	void instancePrefabMulti (bool recursive = false)
	{
		List <NestedPrefab> siblingNestFabInstances;
		
		GameObject instanceRoot = getParentPrefabInst ();

        siblingNestFabInstances = instanceRoot.transform.getComponentsRecursive<NestedPrefab>(true);

		foreach (NestedPrefab nFab in siblingNestFabInstances)
		{
			nFab.instancePrefab();

			if (recursive)
			{
                List <NestedPrefab> subNFabs = nFab.m_prefabInst.transform.getComponentsRecursive<NestedPrefab> (true);

				if (subNFabs.Count > 0)
				{
					subNFabs[0].instancePrefabMulti (true);
				}
			}
		}
	}

	void instancePrefab (GameObject prefab, bool recursive)
	{
		if (!prefab)
		{
			Debug.LogError ("No prefab selected!");
			return;
		}

		if (m_prefabInst)
		{
			removePrefabInstance (true);
		}

		updateParentPrefabInfo ();

		GameObject newObj = (GameObject)
			PrefabUtility.InstantiatePrefab (prefab);

		m_prefabInst = newObj;

		// Apply modifications.
		//PrefabUtility.SetPropertyModifications (newObj,
		                           //             SerialisablePropertyMod.convertArray (m_AppliedPropMods));
		applyProperyModifications ();

		// If this is the first time we've instanced this prefab, transform the scale.
		if (m_LocalPropMods.Count == 0 &&
		    m_parentPropMods.Count == 0)
		{
			newObj.transform.SetParent (transform);
		}
		else
		{
			// Otherwise, don't change scale, etc, when attaching prefab instance.
			newObj.transform.SetParent (transform, false);
		}

		newObj.transform.localPosition = Vector3.zero;
		newObj.transform.localRotation = Quaternion.identity;

		restoreParentInstObjectReferences ();

		reattachedSavedGOTrees ();

		EditorUtility.SetDirty (this);

		if (recursive)
		{
			// Instance child nested prefabs.
            List<NestedPrefab> subNFabs = m_prefabInst.transform.getComponentsRecursive<NestedPrefab>(true);
			
			foreach (NestedPrefab nfab in subNFabs)
			{
				nfab.instancePrefab (true);
			}
		}
	}

	public void revertToPrefab ()
	{
		if (!m_prefab)
		{
			Debug.LogError ("No prefab selected!");
			return;
		}

		removePrefabInstanceMulti ();

		m_LocalPropMods.Clear ();
		m_parentPropMods.Clear ();

		// Remove additional  game objects.
		foreach (GameObjPair objPair in m_detachedObjTrees)
		{
			GameObject.DestroyImmediate (objPair.m_targetObj);
		}

		m_detachedObjTrees.Clear ();

		//  Reset transform
		transform.localRotation = Quaternion.identity;
		transform.localScale = Vector3.one;

		instancePrefabMulti ();

		EditorUtility.SetDirty (this);
		EditorUtility.SetDirty (transform);
	}

	public void revertToParent ()
	{
		if (!m_prefab)
		{
			Debug.LogError ("No prefab selected!");
			return;
		}

		removePrefabInstanceMulti ();

		PrefabUtility.ResetToPrefabState (this);
		PrefabUtility.ResetToPrefabState (transform);

		instancePrefabMulti();

		EditorUtility.SetDirty (this);
	}

	public void applyToPrefab ()
	{
		if (!m_prefab)
		{
			Debug.LogError ("No prefab selected!");
			return;
		}

		if (transform.parent == null)
		{
			Debug.LogError ("Nested prefab must be a child of another prefab instance to apply changes!");
			return;
		}

		checkForParentUpdate ();

		removePrefabInstance (true);

		if (m_LocalPropMods.Count > 0)
		{
			// Set our local property modifications as the new parent modifications that we will propergate.
			m_parentPropMods = new List <SerialisablePropertyMod> (m_LocalPropMods);

			// Local property modifications must ALWAYS be an empty list when propergated.
			m_LocalPropMods = new List <SerialisablePropertyMod> ();
		}

		// Force prefab system to record modified properties NOW.
		PrefabUtility.RecordPrefabInstancePropertyModifications (this);
		EditorUtility.SetDirty (this);

		GameObject instRoot = getParentPrefabInst ();
		GameObject prefab = getParentPrefab();
	
		if (instRoot == null)
		{
			Debug.LogError ("Didn't find prefab instance root!");
			return;
		}

		if (PrefabUtility.GetPrefabType (instRoot) != PrefabType.DisconnectedPrefabInstance && 
		    PrefabUtility.GetPrefabType (instRoot) != PrefabType.PrefabInstance)
		{
			Debug.LogError ("Prefab instance root is of incorrect type!? " + PrefabUtility.GetPrefabType (instRoot));
			return;
		}

		if (prefab == null)
		{
			Debug.LogError ("Didn't find prefab!");
			return;
		}

		if (PrefabUtility.GetPrefabType (prefab) != PrefabType.Prefab)
		{
			Debug.LogError ("prefab not a prefab!?");
		}

		sanitiseSAllNestedPrefabs ();

		// Remove all nested prefab instances attached to the root prefab instance.
		foreach (KeyValuePair <int, NestedPrefab> fabPair in s_AllNestedPrefabs [m_allFabsKey])
		{
			NestedPrefab nestPre = fabPair.Value;
			
			if (nestPre == null)
			{
				Debug.LogWarning ("Bad registered nested prefab (2)!???" );
				continue;
			}

			if (fabPair.Key == GetInstanceID())
			{
				// We don't want to call the default "remove" function on this instance of the script.
				continue;
			}

			if (nestPre.getParentPrefabInst() == instRoot)
			{
				// Don't reconnect nested prefabs attached to the same parent as this one.
				nestPre.removePrefabInstance (true);
			}
			else
			{
				nestPre.removePrefabInstanceMulti();
			}
		}

		//forceRecordAllPropertyChanges ();

	//	PrefabUtility.ReconnectToLastPrefab (instRoot);

		// Perform prefab update.
		PrefabUtility.ReplacePrefab (instRoot, prefab);

		sanitiseSAllNestedPrefabs();

		//return;

		// There was a bug causing mutliple duplicates of detached object trees to
		// spawn on the original object.  This is a dirty work around.
		revertToParent ();

		//m_LocalPropMods = new List <SerialisablePropertyMod> (m_parentPropMods);

		// Notify all prefabs that the prefab has changed.
		foreach (NestedPrefab nFab in s_AllNFabTracker)
		{
			if (nFab == null) // GRAAAAH!  YOU-NIT-EEEE!!
			{
				continue;
			}

			nFab.onParentPrefabUpdate();
		}

		// Reinstantiate the instances.
		foreach (NestedPrefab nestPre in s_AllNestedPrefabs [m_allFabsKey].Values)
		{
			//PrefabUtility.RecordPrefabInstancePropertyModifications(nestPre);
			nestPre.instancePrefab();
		}
	}

	PropertyModification[] getInstModifications ()
	{
		PropertyModification[] mods;
		List <PropertyModification> retMods = new List <PropertyModification> ();

		// Get the properties that differ from the original prefab.
		mods = PrefabUtility.GetPropertyModifications (gameObject);

		if (mods == null)
		{
			Debug.Log ("No parent prefab!");
			return null;
		}

		// Determine which properties belong to our nested prefab.
		foreach (PropertyModification mod in mods)
		{
			if (isObjInHierarchy (mod.target, m_prefabInst.transform))
			{
				retMods.Add (mod);
			}
			else
			{
				Debug.Log ("Skipping property: " + mod.propertyPath + " in: " + mod.target);
			}
		}

		return retMods.ToArray();
	}

	int getModPosWithSamePathIn (SerialisablePropertyMod searchMod,
	                                              List <SerialisablePropertyMod> searchIn)
	{
		//SerialisablePropertyMod retMod;

		for (int i = 0; i < searchIn.Count; ++i)
		{
			if (searchIn [i].target == searchMod.target &&
			    searchIn [i].propertyPath == searchMod.propertyPath)
			{
				return i;
			}
		}

		// Not found.
		return -1;
	}

	void applyProperyModifications()
	{
		List <SerialisablePropertyMod> mods = new List <SerialisablePropertyMod> (m_parentPropMods);
	
		// Apply/Add the local modifications.
		foreach (SerialisablePropertyMod localMod in m_LocalPropMods)
		{
			if (localMod.propertyPath == "m_RootOrder")
			{
				// Skip this one.  Overriding it messes things up.
				Debug.Log ("Skipping root order.");
				continue;
			}

			// Dos this local mod override any parent mods?
			int modPos = getModPosWithSamePathIn (localMod, mods);

			if (modPos != -1)
			{
				// Override mod with local copy.
				mods [modPos] = localMod;
			}
			else
			{
				// Add local mod as new property.
				mods.Add (localMod);
			}
		}

//		// Remove references to "root order".
//		for (int i = 0; i < mods.Count; ++i)
//		{
//			if (mods [i].propertyPath == "m_RootOrder")
//			{
//				Debug.Log ("Skipping root order.");
//				mods.RemoveAt (i);
//			}
//		}

		PrefabUtility.SetPropertyModifications (m_prefabInst,
		                                        SerialisablePropertyMod.convertArray (mods));
	}

	public void saveParentInstObjectReferences ()
	{
		GameObject rootObj = getParentPrefabInst ();

		if (rootObj == null)
		{
			Debug.LogError ("Bad rootObj!");
		}

		if (!m_prefabInst)
		{
			// No prefab to save from.
			return;
		}

		m_parentNFabReferences.Clear ();

		foreach (Object obj in getAllObjectsRecursive (m_prefabInst.transform))
		{
//			if (obj.getType() == typeof (NestedPrefab).FullName)
//			{
//				Debug.Log ("Trans obj: " + obj.getType());
//			}

			List <SerialisablePropertyMod> foundRefs = findObjRefsInHierarchy (obj, rootObj.transform, true);

			//Debug.Log ("Found refs: " + foundRefs.Count);

			m_parentNFabReferences.AddRange (foundRefs);
		}

		EditorUtility.SetDirty (this);
	}

	public void restoreParentInstObjectReferences ()
	{
		GameObject rootObj = getParentPrefabInst ();
		
		if (rootObj == null)
		{
			Debug.LogError ("Bad rootObj!");
		}

		foreach (SerialisablePropertyMod mod in m_parentNFabReferences)
		{
			// Restore the references to the new instanced prefab.
			Object objReference = findInstanceOfPrefabObjIn (m_prefabInst, mod.objectReference);

		//	Debug.Log ("taget mod: " + mod.target.name);
			//Debug.Log ("parent Inst: " + getParentPrefabInst().name);

			//Object targetObject = findInstanceOfPrefabObjIn (getParentPrefabInst(), mod.target);
			Object targetObject = mod.target;
			//Debug.Log ("targetObj: " + targetObject.name);

			SerializedObject serialisedObj = new SerializedObject (targetObject);

			SerializedProperty objProperty = serialisedObj.FindProperty (mod.propertyPath);

			objProperty.objectReferenceValue = objReference;
			serialisedObj.ApplyModifiedProperties();

			EditorUtility.SetDirty (targetObject);

			//Debug.Log ("property: " + objProperty.propertyPath);

		}
	}
	
//	public void resetSavedAttachedGOs ()
//	{
//		for (int i = 0; i < m_detachedObjTrees.Count; ++i)
//		{
//			NestedPrefab fabNest = (NestedPrefab) PrefabUtility.GetPrefabParent (this);
//			
//			GameObjPair thisPair = m_detachedObjTrees [i];
//			GameObjPair fabPair = fabNest.m_detachedObjTrees [i];
//			
//			gameObject.SetActive (true);
//			
//			// Set the saved postions.
//			thisPair.m_position = fabPair.m_position;
//			thisPair.m_rotation = fabPair.m_rotation;
//		}
//	}

	void findNewAttachedGameObjects (GameObject rootObj,
	                                 GameObject prefab,
	                                 List <GameObjPair> newObjects)
	{
		GameObject obj = PrefabUtility.FindPrefabRoot (rootObj);

		if (rootObj == null)
		{
			Debug.LogWarning ("Null attached object!?");
			return;
		}

		if (obj != m_prefabInst)
		{
			GameObject attachPoint = (GameObject) PrefabUtility.GetPrefabParent (rootObj.transform.parent.gameObject);

			Vector3 localPos = rootObj.transform.localPosition;

			newObjects.Add (new GameObjPair (rootObj, attachPoint, localPos, rootObj.transform.localRotation));
			return;
		}

		foreach (Transform trans in rootObj.transform)
		{
			findNewAttachedGameObjects (trans.gameObject, prefab, newObjects);
		}
	}

	public void findNewAttachedGameObjects ()
	{
		findNewAttachedGameObjects (m_prefabInst, m_prefab, m_detachedObjTrees);

		EditorUtility.SetDirty (this);
	}

	void saveNewAttachedGameObjects ()
	{
		List <GameObjPair> newAttGOs = new List <GameObjPair>();
		findNewAttachedGameObjects (m_prefabInst, m_prefab, newAttGOs);

		foreach (GameObjPair objPair in newAttGOs)
		{
			// Parent to this object.
			objPair.m_targetObj.transform.SetParent (transform);
			objPair.m_targetObj.SetActive (false);

			PrefabUtility.ReconnectToLastPrefab (objPair.m_targetObj);

			EditorUtility.SetDirty (objPair.m_targetObj.transform);
			EditorUtility.SetDirty (objPair.m_targetObj);
			EditorUtility.SetDirty (transform);
			EditorUtility.SetDirty (gameObject);

			PrefabUtility.RecordPrefabInstancePropertyModifications (objPair.m_targetObj.transform);
			PrefabUtility.RecordPrefabInstancePropertyModifications (objPair.m_targetObj);
			PrefabUtility.RecordPrefabInstancePropertyModifications (transform);
			PrefabUtility.RecordPrefabInstancePropertyModifications (gameObject);

			m_detachedObjTrees.Add (objPair);
		}

		EditorUtility.SetDirty (this);
		PrefabUtility.RecordPrefabInstancePropertyModifications (this);
	}

	void reattachedSavedGOTrees ()
	{
		foreach (GameObjPair objPair in m_detachedObjTrees)
		{
			if (!objPair.m_targetObj)
			{
				Debug.LogWarning ("Referenced Object lost!");
				continue;
			}

			GameObject attachPoint = (GameObject)
				findInstanceOfPrefabObjIn (m_prefabInst, objPair.m_attachPointOnPrefab);

			if (attachPoint == null)
			{
				Debug.LogWarning ("Failed to find attach point for detached nFab object!");
			}

			objPair.m_targetObj.transform.SetParent (attachPoint.transform);
			objPair.m_targetObj.SetActive (true);

			// Position and rotation are not properly stored in prefabs, so we
			// Store and restore it manually.
			objPair.m_targetObj.transform.localPosition = objPair.m_position;
			objPair.m_targetObj.transform.localRotation = objPair.m_rotation;
		}

		m_detachedObjTrees.Clear ();
	}

	void debugAttachedGos ()
	{
		foreach (GameObjPair objPair in m_detachedObjTrees)
		{
			if (objPair.m_targetObj == null)
			{
				Debug.LogWarning ("Lost obj target!");
				Debug.Break ();
			}
		}
	}

	public void debugPrintModifiedProperies ()
	{
		PropertyModification[] mods = PrefabUtility.GetPropertyModifications (m_prefabInst);

		if (mods == null)
		{
			return;
		}

		foreach (PropertyModification mod in mods)
		{
			Debug.Log (
				"objRef: " + mod.objectReference + "\n" +
			    "path: " + mod.propertyPath  + "\n" +
				"target obj: " + mod.target + "\n" +
				"value: " + mod.value);
		}
	}
}

#endif
