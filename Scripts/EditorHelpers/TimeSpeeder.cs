﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;

using UnityEditor;

public class TimeSpeeder : MonoBehaviour
{
    [Range(0.0f, 10.0f)]
    public float m_timeScale = 1.0f;
		
    void OnValidate()
    {
        setSpeed (m_timeScale);
    }

    public void setSpeed(float timeFactor)
    {
        Time.timeScale = timeFactor;
    }

    // Custom interface to show in editor.
    [CustomEditor(typeof(TimeSpeeder))]
    public class EditorScaleTime : Editor
    {
        public override void OnInspectorGUI()
        {
            TimeSpeeder theScript = (TimeSpeeder) target;

            DrawDefaultInspector();
            if (GUILayout.Button("Reset"))
            {
                //theScript.m_timeScale = 1.0f;
				theScript.setSpeed (1);
            }
            if (GUILayout.Button("Pause"))
            {
                //theScript.m_timeScale = 0;
				theScript.setSpeed (0);
            }
        }
    }
}

#endif
