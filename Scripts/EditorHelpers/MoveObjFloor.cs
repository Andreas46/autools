﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

public class MoveObjFloor: EditorWindow
{
	bool isSnapping = false;
	Vector3 originalPos;

	bool m_ignoreTriggers = true;

	Dictionary <GameObject, int> m_layerMap = new Dictionary <GameObject, int> ();

	[MenuItem ("Window/Snap To Floor")]
	//[MenuItem ("Window/My Window")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		MoveObjFloor window = (MoveObjFloor)EditorWindow.GetWindow (typeof (MoveObjFloor));
		//window.title = "Snap To Floor";
		window.titleContent = new GUIContent ("Snap To Florr");
		window.minSize = new Vector2 (10.0f,10.0f);
		window.Show();
	}
	
	//void Start ()
	MoveObjFloor()
	{
		//Debug.Log ("Initialising Move Obj Floor");
		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}

	// Update is called once per frame
	//void Update () 
	//void OnGUI()

	void OnSceneGUI (SceneView sceneView)
	{
		attemptObjectDrag ();
	}

	void OnGUI()
	{
		BeginWindows();

		m_ignoreTriggers = GUILayout.Toggle (m_ignoreTriggers, "Ignore Triggers");

		GUILayout.BeginHorizontal();

		if (GUILayout.Button("Place On Floor"))
		{
			if (Selection.activeTransform)
			{
			//	Debug.Log ("Snapping");
				isSnapping = true;
				originalPos = Selection.activeTransform.position;

//				Undo.RecordObject (Selection.activeTransform, "Move Object");
			}
		}

		if (GUILayout.Button("Snap To Floor"))
		{
			foreach (GameObject obj in Selection.gameObjects)
			{
				attemptObjectSnap (obj);
			}
		}

		GUILayout.EndHorizontal ();

		checkForCancel ();
		EndWindows();
	}


//	void DoWindow(int unusedWindowID)
//	{
//		GUILayout.Button("Hi");
//		GUI.DragWindow();
//	}

	public void attemptObjectSnap (GameObject obj)
	{
		Vector3 relLowPt = getYExtremeRelative (obj, true);
		
		//Debug.Log ("Lowest Point: " + lowPt);
		Vector3 targetPoint = getDownRayPoint (obj);
		
		Undo.RecordObject (Selection.activeTransform, "Snap Object");
		obj.transform.position = targetPoint - relLowPt;
	}

	void attemptObjectDrag ()
	{
		Transform selectedTrans = Selection.activeTransform;
		//Debug.Log ("Mark");
		if (isSnapping && selectedTrans)
		{
			// If we've clicked, stop snapping.
			if (Event.current.type == EventType.mouseDown && Event.current.button == 0)
			{
				// Drop object.
			//	Debug.Log ("drop");
				isSnapping = false;
				return;
			}

			if (checkForCancel())
			{
				return;
			}
			
//			Vector3? lowPt = getLowestPoint (selectedTrans.gameObject);
//
//			if (!lowPt.HasValue)
//			{
//				lowPt = new Vector3();
//			}
			Vector3 relLowPt = getYExtremeRelative (selectedTrans.gameObject, true);
			
			//Debug.Log ("Lowest Point: " + lowPt);
			Vector3 targetPoint = getMouseRayPoint (selectedTrans.gameObject);

			Undo.RecordObject (Selection.activeTransform, "Move Object");
			selectedTrans.position = targetPoint - relLowPt;
		}
		else
		{
			isSnapping = false;
		}
	}

	bool checkForCancel ()
	{
		// Return object to original position on ESC key.
		if (Event.current.type == EventType.keyUp &&
		    Event.current.keyCode == KeyCode.Escape)
		{
			isSnapping = false;
			Selection.activeTransform.position = originalPos;
			Debug.Log ("cancel snap");
			return true;
		}
		else
		{
			return false;
		}
	}

	Vector3 getYExtremeRelative (GameObject obj, bool getLowestPoint)
	{
		float? extremePt = getYExtreme (obj, getLowestPoint);

		Vector3 retVec = new Vector3();
		
		if (extremePt.HasValue)
		{
			retVec.y = extremePt.Value - obj.transform.position.y;
		}

		return retVec;
	}

	Vector3 getYExtremeVec (GameObject obj, bool getLowestPoint)
	{
		float? extremePt = getYExtreme (obj, getLowestPoint);

		Vector3 retVec = new Vector3();
		
		if (extremePt.HasValue)
		{
			retVec.y = extremePt.Value;
		}

		return retVec;
	}

	float? getYExtreme (GameObject obj, bool getLowestPoint)
	{
		float yExtreme;
		float? retVal = null;

		Collider collider = obj.GetComponent <Collider> ();
		Renderer renderer = obj.GetComponent <Renderer> ();
		if (collider && (!m_ignoreTriggers || !collider.isTrigger))
		{
			if (getLowestPoint)
			{
				yExtreme = -collider.bounds.extents.y;
			}
			else
			{
				yExtreme = collider.bounds.extents.y;  // Get highest Point.
			}

			yExtreme +=  collider.bounds.center.y;

			//Debug.Log ("Y: " + (obj.transform.position - collider.bounds.center).y);
			//lowestPoint.y +=collider.

			retVal = yExtreme;
		}
		else if (renderer)
		{
			if (getLowestPoint)
			{
				yExtreme = -renderer.bounds.extents.y;
			}
			else
			{
				yExtreme = renderer.bounds.extents.y;  // Get highest Point.
			}

			yExtreme += renderer.bounds.center.y;

			retVal = yExtreme;
		}

		float? childExtremeY = null;

		// Compair with lowest point of all children.
		foreach (Transform child in obj.transform)
		{
			childExtremeY = getYExtreme (child.gameObject, getLowestPoint);

			if (childExtremeY.HasValue)
			{
				// If we don't yet have a lowest point
				if (!retVal.HasValue)
				{
					retVal = childExtremeY;
				}

				// If we're getting the lowest point of the child and it is
				// lower that our current lowest point.
				if ((getLowestPoint && childExtremeY.Value < retVal.Value) ||
				    // Or we're getting the highest, and it is higher that the current point.
				      !getLowestPoint && childExtremeY.Value > retVal.Value)
				{
					retVal = childExtremeY;
				}
			}
		}

		return retVal;
	}

	void enableIgnoreRaycast (GameObject obj)
	{
		m_layerMap [obj] = obj.layer;
		obj.layer = LayerMask.NameToLayer ("Ignore Raycast");
		
		// And the same for all children.
		foreach (Transform child in obj.transform)
		{
			enableIgnoreRaycast (child.gameObject);
		}
	}

	void disableIgnoreRaycast (GameObject obj)
	{
		obj.layer = m_layerMap [obj];
		m_layerMap.Remove (obj);
		
		// And the same for all children.
		foreach (Transform child in obj.transform)
		{
			disableIgnoreRaycast (child.gameObject);
		}
	}

	Vector3 getMouseRayPoint (GameObject selectedObj)
	{
//		Vector3 retVec;

		if (!Camera.current)
		{
			return new Vector3();
		}

		Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);

		return getRayPoint (selectedObj, ray);
	}

	Vector3 getDownRayPoint (GameObject obj)
	{
		Vector3 highPt = getYExtremeRelative (obj, false);
		Ray ray = new Ray (highPt + obj.transform.position, Vector3.down);

		return getRayPoint (obj, ray);
	}

	Vector3 getRayPoint (GameObject selectedObj, Ray ray)
	{
		Vector3 retVec = new Vector3 ();

		// Ignore selected object in ray cast.
		enableIgnoreRaycast (selectedObj);

		RaycastHit hitInfo;

		//if (Physics.Raycast (castStart, targetDirection, out hitInfo))
		if (Physics.Raycast (ray, out hitInfo))
		{
			//Debug.Log ("Target Position: " + hitInfo.point);
			retVec = hitInfo.point;
		}

		// Restore objects original layer.
		disableIgnoreRaycast (selectedObj);

		return retVec;
	}
}

#endif
