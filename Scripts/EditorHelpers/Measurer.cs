﻿// Only compile when using the editor.
#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

using AuTools;

public class Measurer : EditorWindow
{
	[MenuItem ("Window/Measure Things")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        Measurer window = (Measurer) EditorWindow.GetWindow (typeof (Measurer));
        //window.titleContent = new GUIContent ("unitometer");
        window.Show();
    }

    void Update ()
    {

    }

    Measurer()
    {
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }

    void OnSceneGUI(SceneView sceneView)
    {
        Repaint();
    }

    void OnGUI()
    {
        BeginWindows();

        GUILayout.Label("Global Measurments");

		GUILayout.BeginHorizontal();

        Vector3 dimensions = getObjectGlobalDimensions();
        GUILayout.Label ("W: " + dimensions.x);
        GUILayout.Label ("H: " + dimensions.y);
        GUILayout.Label ("D: " + dimensions.z);


    //    GUILayout.Label (getObjectGlobalDimensions().ToString());

        GUILayout.EndHorizontal();

        EndWindows();
    }

    Vector3 getObjectGlobalDimensions ()
    {
        if (Selection.activeTransform)
        {
            GameObject[] selectedObjs = Selection.gameObjects;

            Bounds bounds = ObjMeasurments.getGlobalBounds (selectedObjs);

            return bounds.size;
        }
        else
        {
            return Vector3.zero;
        }
    }
}

#endif
