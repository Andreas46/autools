﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif



[ExecuteInEditMode]
public class DrawCollider: MonoBehaviour
{
	public enum ColType
	{
		None,
		Sphere,
		Box,
		Capsule
	}


	public Color m_colour = Color.white;

#if UNITY_EDITOR

	ColType colType = ColType.None;

	void Awake ()
	{
		Collider col = GetComponent <Collider> ();

		if (col)
		{
			//Debug.Log ("aoeou");

			if (col is SphereCollider)
			{
				colType = ColType.Sphere;
			}
			else if (col is BoxCollider)
			{
				colType = ColType.Box;
			}
			else if (col is CapsuleCollider)
			{
				colType = ColType.Capsule;
			}
		}
	}

	void OnDrawGizmos ()
	{
        Gizmos.color = m_colour;
		Handles.color = m_colour;

        drawCollider();
	}

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
		Handles.color = Color.green;
        drawCollider();
    }

    void drawCollider()
    {
        //Gizmos.matrix = Matrix4x4.TRS (Vector3.zero, transform.r, new Vector3 (1,1,1));
        Gizmos.matrix = transform.localToWorldMatrix;

		if (colType == ColType.Box)
		{
			//Gizmos.DrawWireCube(GetComponent <Collider>().bounds.center, Vector3.Scale (GetComponent <BoxCollider>().size, transform.lossyScale)); 
			Gizmos.DrawWireCube (GetComponent<BoxCollider> ().center, GetComponent<BoxCollider> ().size);
		}
		else if (colType == ColType.Sphere)
		{
			Gizmos.DrawWireSphere (GetComponent <SphereCollider> ().center, GetComponent <SphereCollider> ().radius);
			Handles.DrawWireDisc (transform.position, -Camera.current.transform.forward, GetComponent <SphereCollider> ().radius);
			//Handles.DrawSolidDisc (GetComponent <SphereCollider>().center, Vector3.back, GetComponent <SphereCollider>().radius);
			//UnityEditor.Handles.DrawSolidDisc (transform.position, Vector3.up,  GetComponent <SphereCollider>().radius);
		}
		else if (colType == ColType.Capsule)
		{
			CapsuleCollider capCol = GetComponent <CapsuleCollider> ();

			float radius = capCol.radius;
			Vector3 capCentre = capCol.center;
			Vector3 capEnd = new Vector3 (0, capCol.height / 2.0f - radius, 0);

			// Draw Top of cap.
			Gizmos.DrawWireSphere (capCentre + capEnd, radius);

			// Draw Bottom of cap.
			Gizmos.DrawWireSphere (capCentre - capEnd, radius);

			// Draw "Middle" Bit.
			Vector3 edgeStart = capCentre + capEnd;
			Vector3 edgeEnd = capCentre - capEnd;
			Gizmos.DrawLine (edgeStart + new Vector3 (radius, 0, 0), edgeEnd + new Vector3 (radius, 0, 0));
			Gizmos.DrawLine (edgeStart + new Vector3 (-radius, 0, 0), edgeEnd + new Vector3 (-radius, 0, 0));
			Gizmos.DrawLine (edgeStart + new Vector3 (0, 0, radius), edgeEnd + new Vector3 (0, 0, radius));
			Gizmos.DrawLine (edgeStart + new Vector3 (0, 0, -radius), edgeEnd + new Vector3 (0, 0, -radius));
		}
		 
        Gizmos.matrix = Matrix4x4.identity;
    }

	#endif
}
	
