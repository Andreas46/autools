﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;


public class CameraManagerPreview : EditorWindow
{
	public Camera m_camera;

	[MenuItem ("Window/Preview Cameras")]

	static void Init()
	{
		// Get existing open window or if none, make a new one:
		CameraManagerPreview window = (CameraManagerPreview) EditorWindow.GetWindow (typeof (CameraManagerPreview));
		window.titleContent = new GUIContent ("Camera Previews");
		//window.title = "Camera Previews";

		window.autoRepaintOnSceneChange = true;
		window.Show();
	}

	CameraManagerPreview()
	{
		//SceneView.onSceneGUIDelegate += OnSceneGUI;
	}


	//void OnSceneGUI (SceneView sceneView)
	void OnGUI()
	{
		//BeginWindows();
		//GUILayout.BeginVertical();

		m_camera = EditorGUILayout.ObjectField (m_camera, typeof (Camera),true) as Camera;

		//GUILayout.Box ("");



		if (Event.current.type == EventType.Repaint && m_camera)
		{
			float camAspect = m_camera.aspect;
			float width = this.position.width - 25;
			float height = width / camAspect;

			Rect renderArea = new Rect (10, 0, width, height);

			//GUILayout.BeginArea (renderArea);
			//GUILayout.Box ("ee");

			//Rect camPreview = GUILayoutUtility.GetLastRect ();
			Rect camPreview = renderArea;

			//Handles.BeginGUI();

			Handles.SetCamera (m_camera);
			m_camera.pixelRect = camPreview;
			m_camera.Render(); // Render to window.

			//Handles.DrawCamera (camPreview, m_camera, DrawCameraMode.Normal);

			//Handles.EndGUI();

			this.Repaint ();

			//GUILayout.EndArea();
		}
//
	//	GUILayout.EndVertical();
		//EndWindows();
	}
//
//	void OnGUI()
//	{
//
//	}
}

#endif
