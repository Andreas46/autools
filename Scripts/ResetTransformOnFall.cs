﻿using UnityEngine;
using System.Collections;

public class ResetTransformOnFall: MonoBehaviour
{
	Vector3 m_startPos;
	Quaternion m_startRot;

	public float m_resetHeight = -10.0f;

	void Awake ()
	{
		m_startPos = GetComponent <Transform> ().position;
		m_startRot = GetComponent <Transform> ().rotation;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (transform.position.y < m_resetHeight)
		{
			resetTransform();
		}
	}

	void resetTransform ()
	{
		GetComponent <Transform> ().position = m_startPos;
		GetComponent <Transform> ().rotation = m_startRot;

		// Reset RigidBody.
		Rigidbody rBody = GetComponent <Rigidbody>();
		if (rBody)
		{
			rBody.velocity = Vector3.zero;
			rBody.angularVelocity = Vector3.zero;
		}
	}
}
