﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
//using UnityEditor;
//
//[CustomEditor (typeof (UidComponent))]
//public class EditorUidComponent : Editor
//{
//	public override void OnInspectorGUI()
//	{
//		UidComponent theScript = (UidComponent) target;
//
//		DrawDefaultInspector();
//
//		if (GUILayout.Button ("Reassign All Uids"))
//		{
//			theScript.m_timeScale = 1.0f;
//		}
//	}
//}
//
//#endif

[ExecuteInEditMode]
[DisallowMultipleComponent]
public class UidComponent: MonoBehaviour
{
	[SerializeField] string m_idGroup = "None";
	public string IdGroup { get { return m_idGroup; } 
		set
		{
			if (m_idGroup == value)
			{
				return;
			}

			if (m_uid != 0)
			{
				UidAllocator.Inst.returnUid (m_idGroup, m_uid);
			}

			// Unregister in the current group.
			UidAllocator.Inst.unRegisterComponent (this);

			m_idGroup = value;

			Uid = UidAllocator.Inst.allocateUid (m_idGroup);

			// Reregister.
			UidAllocator.Inst.registerComponent (this);
		}
	}

	[SerializeField] int m_uid = 0;
	public int Uid { get { return m_uid; }
		set
		{
#if UNITY_EDITOR
			Undo.RecordObject (this, "Uid Seting to " + value);
#endif
			m_uid = value;	

		//	EditorUtility.SetDirty (this);
		}
	}

	void Awake ()
	{
		UidAllocator.Inst.registerComponent (this);
	}

	void Start ()
	{
		if (m_uid == 0)
		{
			//Debug.Log ("Allocating uid to zero value: ", this);
			Uid = UidAllocator.Inst.allocateUid (IdGroup);
		}
	}

	void OnDestroy ()
	{
		if (UidAllocator.Inst != null)
		{
			UidAllocator.Inst.unRegisterComponent (this);
		}
	}

	//public override void OnInspectorGUI()
	//{
	//	SerializedProperty myProperty =  FindProperty("name");
	//	EditorGUILayout.PropertyField(myProperty);
	//
	//	serializedObject.ApplyModifiedProperties();
	//}
}
