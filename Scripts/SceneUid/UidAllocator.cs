﻿using UnityEngine;
using System.Collections.Generic;

using System.Linq;

#if UNITY_EDITOR

using UnityEditor;

[CustomEditor (typeof (UidAllocator))]
public class EditorUidUidAllocator: Editor
{
	public override void OnInspectorGUI()
	{
		UidAllocator theScript = (UidAllocator) target;

		DrawDefaultInspector();

		GUILayout.BeginHorizontal ();

		if (GUILayout.Button ("Reassign All Uids"))
		{
			theScript.reassignAllUids ();
		}

		if (GUILayout.Button ("Clear All Uids"))
		{
			theScript.clearAllUids ();
		}

		GUILayout.EndHorizontal ();
	}
}

#endif

[ExecuteInEditMode]
public class UidAllocator: MonoBehaviour
{
	static UidAllocator s_inst;
	public static UidAllocator Inst { get { return s_inst; } }

	public static List <string> s_groups = new List <string>();

	Dictionary <string, int> m_nextId = new Dictionary <string, int>();

	Dictionary <string, List <int>> m_idCache = new Dictionary <string, List <int>>();

	Dictionary <string, List <UidComponent>> m_allTheComps =
		new Dictionary <string, List <UidComponent>> ();

	public static UidComponent getComponentById (string idGroup, int uid)
	{
		foreach (UidComponent comp in Inst.m_allTheComps [idGroup])
		{
			if (comp.Uid == uid)
			{
				return comp;
			}
		}

		Debug.LogError ("Falied to lookup UidComponent with id: " + uid);
		return null;
	}

	void Awake ()
	{
		s_inst = this;	
	}

	void OnDestroy ()
	{
		s_inst = null;
	}

	public void registerComponent (UidComponent comp)
	{
		if (!m_allTheComps.ContainsKey (comp.IdGroup))
		{
			m_allTheComps [comp.IdGroup] = new List<UidComponent> ();
		}

		// Update the maximum allocated view id.
		if (comp.Uid > 0)
		{
			if (isUidRegistered (comp.Uid))
			{
				Debug.Log ("Uid " + comp.Uid +" registered. Resetting");

				// Uid already registered.  Reset to zero.
				comp.Uid = 0;
				return;
			}

			if (m_nextId.ContainsKey (comp.IdGroup))
			{
				m_nextId [comp.IdGroup] = Mathf.Max (m_nextId [comp.IdGroup], comp.Uid + 1);
			}
			else
			{
				if (!s_groups.Contains (comp.IdGroup))
				{
					s_groups.Add (comp.IdGroup);
				}

				m_nextId [comp.IdGroup] = comp.Uid + 1;
			}
		}

		m_allTheComps [comp.IdGroup].Add (comp);
	}

	public void unRegisterComponent (UidComponent comp)
	{
		Debug.Assert (m_allTheComps.ContainsKey (comp.IdGroup),
			"Can't unregister UidComponent; invalid group: " + comp.IdGroup);
		Debug.Assert (m_allTheComps [comp.IdGroup].Contains (comp), "Uidcomp not registed in group: " + comp.IdGroup);

		m_allTheComps [comp.IdGroup].Remove (comp);
	}

	public int allocateUid (string idGroup)
	{
		int uid;

		// Try to get a cached value.
		uid = tryGetCachedId (idGroup);
		if (uid != 0)
		{
			Debug.Log ("Allocating cached uid: " + uid);

			return uid;
		}

		if (!m_nextId.ContainsKey (idGroup))
		{
			// No uid's allocated to this group yet.  Start from 1.
			m_nextId [idGroup] = 1;
		}

		// Return next available uid.
		uid = m_nextId [idGroup];
		m_nextId [idGroup]++;

		Debug.Log ("Allocating new uid: " + uid);

		return uid;
	}

	public void returnUid (string idGroup, int id)
	{
		if (!m_idCache.ContainsKey (idGroup))
		{
			m_idCache [idGroup] = new List<int> ();
		}

		m_idCache [idGroup].Add (id);
	}
		
	public void reassignAllUids ()
	{
		resetUidCounters();

		// Allocate new uids.
		foreach (KeyValuePair <string, List <UidComponent>> compList in m_allTheComps)
		{
			foreach (UidComponent comp in compList.Value)
			{
				comp.Uid = allocateUid (comp.IdGroup);
			}
		}
	}

	public void clearAllUids ()
	{
		resetUidCounters ();

		// setAllUidsToZero.
		foreach (KeyValuePair <string, List <UidComponent>> compList in m_allTheComps)
		{
			foreach (UidComponent comp in compList.Value)
			{
				comp.Uid = 0;
			}
		}
	}

	void resetUidCounters ()
	{
		// Clear cache.
		foreach (KeyValuePair <string, List <int>> cachPair in m_idCache)
		{
			cachPair.Value.Clear ();
		}

		// Reset next ids.
		foreach (KeyValuePair <string, int> idPair in m_nextId.ToArray())
		{
			m_nextId [idPair.Key] = 1;
		}
	}

	int tryGetCachedId (string idGroup)
	{
		// If there is no cached id for this group.
		if (!m_idCache.ContainsKey (idGroup) || m_idCache [idGroup].Count == 0)
		{
			return 0;
		}

		// Lookup last added id.
		int retVal = m_idCache [idGroup] [m_idCache [idGroup].Count - 1];

		// remove last added id from cache.
		m_idCache [idGroup].RemoveAt (m_idCache [idGroup].Count - 1);

		return retVal;
	}

	bool isUidRegistered (int uid)
	{
		foreach (KeyValuePair <string, List <UidComponent>> compList in m_allTheComps)
		{
			foreach (UidComponent comp in compList.Value)
			{
				if (comp.Uid == uid)
				{
					return true;
				}
			}
		}

		return false;
	}
}
