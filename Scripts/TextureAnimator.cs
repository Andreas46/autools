﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

// Replace "TextureAnimatorEditor"
// Replace "TextureAnimator" with the class to be edited.

[CustomEditor (typeof (TextureAnimator))]
[CanEditMultipleObjects]
public class TextureAnimatorEditor : Editor
{
	//int m_x;
	//int m_y;
	Vector2 m_activeFrame;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		GUILayout.BeginHorizontal();
		
		//m_x = EditorGUILayout.IntField  ("X", m_x);
		//m_y = EditorGUILayout.IntField ("Y", m_y);
		m_activeFrame = EditorGUILayout.Vector2Field ("Frame", m_activeFrame);

		if (GUILayout.Button ("SetFrame"))
		{
			foreach (TextureAnimator script in targets)
			{
				script.setFrame ((int) m_activeFrame.x, (int) m_activeFrame.y);
			}
		}
		
		GUILayout.EndHorizontal();
	}
	
}

#endif

public class TextureAnimator : MonoBehaviour
{
	public float m_cellWidth;
	public float m_cellHeight;



	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void setFrame (int x, int y)
	{
		Material mat = GetComponent <Renderer>().material;

		int numFramesY = (int)Mathf.Round (1.0f / m_cellHeight);

		Vector2 newOffset = new Vector2 (m_cellWidth * x,  m_cellHeight * (numFramesY - y - 1));

		Debug.Log ("num frames y: " + numFramesY);
		Debug.Log ("offset: " + newOffset);

		mat.SetTextureOffset ("_MainTex", newOffset);
	}
}
