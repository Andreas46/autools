﻿using UnityEngine;
using System.Collections;

public class ScaleBackground : MonoBehaviour {

	int m_lastScreenWidth;
	int m_lastScreenHeight;

	[Tooltip ("10 for a Unity3d plane.")]
	public float lUnityPlaneSize = 10.0f;

	[Tooltip("As a ratio (0-1) how far away the plane is renderd.  0 is nearest, 1 is furthest.")]
	public float m_planeZRatio = 1.0f;

	[Tooltip ("The Camera this background will scale to.")]
	public Camera m_cam;

	float m_planeZ;

	void Start ()
	{
		// Enabled Renderer.
		GetComponent <Renderer> ().enabled = true;

		calculatePlaneZPos ();
	}

	void Update()
	{
		if (m_lastScreenWidth != Screen.width ||
		    m_lastScreenHeight != Screen.height)
		{
			UpdateBackgroundPos ();
		
			m_lastScreenWidth = Screen.width;
			m_lastScreenHeight = Screen.height;
		}
	}

	public void calculatePlaneZPos ()
	{
		Camera cam = m_cam;

		float planeFarZ = (cam.farClipPlane - 0.1f);
		float planeNearZ = (cam.nearClipPlane + 0.01f);

		m_planeZ = ((planeFarZ - planeNearZ) * m_planeZRatio) + planeNearZ;
	}

	public void UpdateBackgroundPos()
	{
		Camera cam = m_cam;

		if (cam.orthographic)
		{
			float lSizeY = cam.orthographicSize * 2.0f;
			float lSizeX = lSizeY *cam.aspect;
			transform.localScale = new Vector3 (lSizeX / lUnityPlaneSize, lSizeY / lUnityPlaneSize, 1);
		}
		else
		{
			//float lPosition = (cam.farClipPlane - 0.1f);
			transform.position = cam.transform.position + cam.transform.forward * m_planeZ;
			//transform.LookAt (cam.transform);
			//transform.Rotate (90.0f, 0.0f, 0.0f);
			
			float h = (Mathf.Tan (cam.fieldOfView * Mathf.Deg2Rad * 0.5f) * m_planeZ * 2f) / lUnityPlaneSize;

			Texture tex = GetComponent <Renderer>().sharedMaterial.GetTexture("_MainTex");

			float texAspect = tex.width / tex.height;

			//Debug.Log ("aspect: " + texAspect);

			transform.localScale = new Vector3 (h * texAspect, h, 1.0f);
		}
	}

}