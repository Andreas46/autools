﻿// Only compile when using the editor.
#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (ScaleBackground))]
public class EditorScaleBackground : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		ScaleBackground myScript = (ScaleBackground)target;

		if(GUILayout.Button ("Scale Background"))
		{
			Undo.RecordObject  (myScript.transform, "Scaling of Background");
			myScript.calculatePlaneZPos();
			myScript.UpdateBackgroundPos();
		}
	}
}

#endif
