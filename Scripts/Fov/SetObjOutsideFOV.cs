﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor (typeof(SetObjOutsideFOV))]
[CanEditMultipleObjects]
public class EditorSetObjOutsideFOV : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        //SetObjOutsideFOV theScript = (SetObjOutsideFOV) target;

        if (GUILayout.Button ("Set Position"))
        {
            //theScript.setObjOutsideFOV (theScript.m_ScaleingFactor.x, theScript.m_ScaleingFactor.y);
            foreach (SetObjOutsideFOV script in targets)
            {
                script.setObjOutsideFOV (script.m_ScaleingFactor.x, script.m_ScaleingFactor.y, Camera.main);
            }
        }
    }
}

#endif

public class SetObjOutsideFOV : MonoBehaviour {

    Camera m_camera;

    public float m_padding = 2;
    public Vector2 m_ScaleingFactor = new Vector2 ();

	// Use this for initialization
	void Start () {
        m_camera = Camera.main;

        // Debug.Log("Obj Depth: " + getObjCamDepth());
        setObjOutsideFOV (m_ScaleingFactor.x, m_ScaleingFactor.y, m_camera);

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    float getObjCamDepth (Camera cam)
    {
        return (transform.position - cam.transform.position).z - cam.nearClipPlane;
    }

    public SetObjOutsideFOV (Vector2 factors, Camera cam)
    {
        setObjOutsideFOV (factors.x, factors.y, cam);
    }

    public void setObjOutsideFOV (float xFactor, float yFactor, Camera cam)
    {
        float objDepth = getObjCamDepth (cam);

        float hExtent = (Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad * 0.5f) * objDepth);
        float wExtent = hExtent * cam.aspect;
 
        Vector3 newPos = transform.position;

        if (yFactor != 0)
        {
            // New Y pos is the camera's pos * the distance to the edje of FOX scaled by "directions".
            newPos.y = cam.transform.position.y + (hExtent * yFactor);
            // Add the padding;
            newPos.y += m_padding * yFactor;
        }
        else
        {
            // Don't change the yPos if the factor is 0.
            newPos.y = transform.position.y;
        }

        if (xFactor != 0)
        {
            newPos.x = cam.transform.position.x + (wExtent * xFactor);
            // Add the padding;
            newPos.x += m_padding * xFactor;
        }
        else
        {
            // Don't change the xPos if the factor is 0.
            newPos.x = transform.position.x;
        }

#if UNITY_EDITOR
        Undo.RecordObject(transform, "Set Spawner Pos");
#endif

        transform.position = newPos;
    }
}
