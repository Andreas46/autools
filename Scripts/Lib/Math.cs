﻿using UnityEngine;
using System.Collections;

namespace AuTools
{
	public delegate float StepFunc (float t);

	static public class AMath
	{
		static public float smoothStep (float t)
		{
			float x = Mathf.Clamp (t, 0.0f, 1.0f); 

			// Evaluate polynomial.
			return x * x * (3 - 2 * x);
		}

		static public float smootherStep (float t)
		{
			float x = Mathf.Clamp (t, 0.0f, 1.0f); 
			
			// Evaluate polynomial.
			return x * x * x * (x * (x * 6.0f - 15.0f) + 10.0f);
		}

		static public float smoothStep (float start, float end, float t, StepFunc stepFunc = null)
		{
			// Check for null -- meaning default -- step function.
			if (stepFunc == null)
			{
				stepFunc = smoothStep;
			}

			float range = (end - start);

			// Scale, unbias and saturate x to 0..1 range
			//float x = Mathf.Clamp ((val - start) / range, 0.0f, 1.0f); 

			//float x = smoothStep (t);
			//float x = smootherStep (t);
			float x = stepFunc (t);

			// Scate up, and bias.
			return (x * range) + start;
		}
	}

	static public class AMathVec3
	{
		// Quadratic bezier curve
		static public Vector3 bezier (float t, Vector3 p1, Vector3 p2, Vector3 p3)
		{
			// Ensure t (ratio) remains between 0 and 1.
			t = Mathf.Clamp (t, 0, 1);

			// Lerp between p1 & p2, then between p2 & p3.  Finally, Lerp along this
			// new line to get the point along the quadratic bezier curve.
			return Vector3.Lerp (Vector3.Lerp(p1, p2, t), Vector3.Lerp(p2, p3, t), t);
		}

		static public Vector3 smoothStep (Vector3 start, Vector3 end, float t, StepFunc stepFunc = null)
		{
			Vector3 retVec = new Vector3 ();

			retVec.x = AMath.smoothStep (start.x, end.x, t, stepFunc);
			retVec.y = AMath.smoothStep (start.y, end.y, t, stepFunc);
			retVec.z = AMath.smoothStep (start.z, end.z, t, stepFunc);

			return retVec;
		}

		static public Vector3 smoothSlerp (Vector3 start, Vector3 target, float t, StepFunc stepFunc = null)
		{
			float smoothT;

			if (stepFunc != null)
			{
				smoothT = stepFunc (t);
			}
			else
			{
				smoothT = AMath.smoothStep (t);
			}

			return Vector3.Slerp (start, target, smoothT);
		}

		static public Quaternion smoothSlerp (Quaternion start, Quaternion target, float t, StepFunc stepFunc = null)
		{
			float smoothT;

			if (stepFunc != null)
			{
				smoothT = stepFunc (t);
			}
			else
			{
				smoothT = AMath.smoothStep (t);
			}
			
			return Quaternion.Slerp (start, target, smoothT);
		}
	}
}
