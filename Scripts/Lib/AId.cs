﻿using UnityEngine;
using System.Collections;

namespace AuTools
{
	static public class AId
	{
		static public string getObjPath (this Object obj)
		{
			string path = ""; 

			if (obj is Component)
			{
				Component comp = obj as Component;

				path += getGameObjPath (comp.gameObject);
			}
			else if (obj is GameObject)
			{
				return getGameObjPath (obj as GameObject);
			}

			path += "/" + obj.GetType ();

			return path;
		}

		static public string getGameObjPath (this GameObject gObj)
		{
			string path = "";

			Transform gObjParent = gObj.transform.parent;

			if (gObjParent)
			{
				path += getGameObjPath (gObjParent.gameObject);
			}

			path += "/" + gObj.name;

			return path;
		}
	}
}