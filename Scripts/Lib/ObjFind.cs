﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AuTools
{
	static public class ObjFind
	{
		static public string getTypeName (this Object obj)
		{
			Regex rx = new Regex (@".*\(([A-z.]*)\)");

			// Find matches.
			//MatchCollection matches = 
			string retStr = rx.Replace (obj.ToString (), "$1");

	//		if (matches.Count == 0)
	//		{
	//			return "no matches in: " + obj.ToString();
	//		}

	//		return matches[0].Groups[0].Value;
			return retStr;
		}

	    static public List<T> getComponentsRecursive<T>(this Transform searchTrans, bool stopAtFirstHit = false, List<T> outList = null)
	    {
	        if (outList == null)
	        {
	            outList = new List<T>();
	        }

	        T comp = searchTrans.GetComponent<T>();

			if (comp != null && !comp.Equals (null))
	        {
				//Debug.Log ("Found component: " + comp);
				//Debug.Log ("EqualNull: " + comp == null);


	            outList.Add (comp);

	            if (stopAtFirstHit)
	            {
	                return outList;
	            }
	        }

	        // Recurse deeper into the hierachy.
	        foreach (Transform trans in searchTrans)
	        {
	            getComponentsRecursive<T>(trans, stopAtFirstHit, outList);
	        }

	        return outList;
	    }

		static public T getComponentRecursive<T> (this Transform searchTrans)
		{
			List <T> retList = getComponentsRecursive<T> (searchTrans, true);

			if (retList.Count == 0)
			{
				return default (T);
			}
			else
			{
				return retList [0];
			}
		}

		static public Transform findRecursive (this Transform searchIn, string name)
		{
			Transform retTrans = searchIn.Find (name);

			// Is the game object with name "name" a direct child?
			if (retTrans)
			{
				return retTrans;
			}

			// recurse deeper to search grandchildren.
			foreach (Transform trans in searchIn)
			{
				retTrans = findRecursive (trans, name);

				if (retTrans)
				{
					return retTrans;
				}
			}

			// Did not find the transform with "name".
			return null;
		}

		static public Transform findWithTag (this Transform searchIn, string tag)
		{
			if (searchIn.tag == tag)
			{
				return searchIn;
			}
			
			// Search children for this tag.
			foreach (Transform trans in searchIn)
			{
				Transform retTrans = findWithTag (trans, tag);
				
				if (retTrans)
				{
					return retTrans;
				}
			}
			
			// Did not find the transform with "name".
			return null;
		}

		static public T findParentWithComponent <T> (this Transform searchFrom)
		{
			Transform parent = searchFrom.parent;

			while (parent != null)
			{
				T comp = parent.GetComponent <T>();

				if (comp != null)
				{
					return comp;
				}
				parent = parent.parent;
			}

			// Not found.
			return default (T);
		}
	}  // class
}  // Namespace