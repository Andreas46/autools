﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//  This is the class that cast's a ray to determine when something
// is obfuscating the player/etc.
public class FaderTrigger : MonoBehaviour 
{
	[Tooltip("The prefab with a box collider, and an instace of 'Fader Enabler' attached")]
	public GameObject m_faderEnablerPrefab;
	[Tooltip("The target which we cast a ray at. I.E. the player")]
	public GameObject m_playerFeet;
	[Tooltip("The object to parent spawned Fader Enablers to (optional)")]
	public GameObject m_enablersParent; 

	Dictionary <Fader, GameObject> m_faderEnablers = new Dictionary <Fader, GameObject>();

	void Start ()
	{
		//m_enablersParent = GameObject.Find ("FaderEnablers");
	}

	void FixedUpdate () 
	{
		handleFeetObfuscatedByFadable ();
	}

	void handleFeetObfuscatedByFadable ()
	{
		//Vector3 castStart = Camera.main.transform.position;
        Vector3 castStart = CameraManager.mainCamera.transform.position;
		Vector3 targetDirection = m_playerFeet.transform.position - castStart;
		float targetDistance = targetDirection.sqrMagnitude;
		targetDirection.Normalize ();
		
		RaycastHit[] hitInfos;
		Dictionary <Fader, GameObject> newEnablers = new Dictionary <Fader, GameObject>();

		hitInfos = Physics.RaycastAll (castStart, targetDirection, targetDistance);

		// Iterate over all hit objects.
		foreach (RaycastHit hit in hitInfos)
		{
			Fader fader = hit.collider.gameObject.GetComponent <Fader>();
			if (fader)  // If this object has an instace of "Fader" attached to it.
			{
				GameObject enabler;
	
				// If we've already registered this fader.
				if  (m_faderEnablers.ContainsKey (fader))
				{
					enabler = m_faderEnablers [fader] as GameObject;
					m_faderEnablers.Remove (fader);

					if (m_enablersParent)
					{
						enabler.transform.SetParent (m_enablersParent.transform);
					}
				}
				else
				{
					// Spawn fader enabler at this object's location.
					enabler = GameObject.Instantiate (m_faderEnablerPrefab, fader.transform.position, transform.rotation) as GameObject;
				}

				newEnablers.Add (fader,  enabler); // Add fader to new list.

				// Move the faderEnabler to were the ray hit the object.
				enabler.transform.position = hit.point;
			}
		}

		// Any remaining faderEnablers in m_FaderEnablers are not longer needed, an can be destroyed.
		foreach (GameObject enabler in m_faderEnablers.Values)
		{
			GameObject.Destroy (enabler);
		}

		m_faderEnablers = newEnablers;
	}
}
