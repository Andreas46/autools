﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using AuTools;

public class Fader : MonoBehaviour {

	public bool m_fadeIn = true;
	public float m_fadeSpeed = 1.0f;

	public float m_minAlpha = 0.3f;
	float m_maxAlpha = 1.0f;

	List <Material> m_materials = new List <Material>();

	// Use this for initialization
	void Start ()
	{
	        List <Renderer> rendList = transform.getComponentsRecursive <Renderer> ();

	       // Debug.Log("Num renderers" + rendList.Count);

	        foreach (Renderer rend in rendList)
	        {
	            if (rend)
	            {
	                //Debug.Log ("Adding renderer" + rendList.Count);

	                m_materials.Add(rend.material);
	            }
	        }
	}
	
	// Update is called once per frame
	void Update ()
	{
	        //gameObject.GetComponentRecursive <>

	        foreach (Material renderMat in m_materials)
	        {
	            fadeStep (renderMat);
	        }
	}

	void fadeStep (Material renderMat)
	{
		//Material renderMat = GetComponent <Renderer> ().material;
		Color colour = renderMat.color;
		
		if ((!m_fadeIn && colour.a <= m_minAlpha) ||
		    m_fadeIn && colour.a >= m_maxAlpha)
		{
			return; // Already faded out.
		}
		
		if (renderMat.GetFloat ("_Mode") != 2.0f)
		{
			// Set the rendertype to "fade".
			renderMat.SetFloat ("_Mode", 2.0f);
			renderMat.SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			renderMat.SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			renderMat.SetInt ("_ZWrite", 0);
			renderMat.DisableKeyword ("_ALPHATEST_ON");
			renderMat.EnableKeyword ("_ALPHABLEND_ON");
			renderMat.DisableKeyword ("_ALPHAPREMULTIPLY_ON");
			renderMat.renderQueue = 3000;
		}
	
		// Set alpha.
		if (m_fadeIn)
		{
			colour.a += m_fadeSpeed * Time.deltaTime;

			if (colour.a > m_maxAlpha)
			{
				colour.a = m_maxAlpha;
			}
		}
		else
		{
			colour.a -= m_fadeSpeed * Time.deltaTime;

			if (colour.a < m_minAlpha)
			{
				colour.a = m_minAlpha;
			}
		}
				
		renderMat.SetColor ("_Color", colour);
	}
}
