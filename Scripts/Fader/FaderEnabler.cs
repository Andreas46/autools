using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FaderEnabler : MonoBehaviour 
{
	public bool m_enableFading = true;

	[Tooltip ("This value is overridden by 'Fader Trigger'")]
	public float m_fadeTime = 1.0f;
	bool m_lastEnableFadeing = true;
	
	float m_minAlpha = 0.3f;  // Minimum visibility as a percentage.
	HashSet <Fader> m_fadedObjects = new HashSet<Fader>();

	static Dictionary <Fader, int> m_faderCount = new Dictionary <Fader, int>();

	void Update () 
	{
		if  (m_enableFading != m_lastEnableFadeing)
		{
			if (m_enableFading)
			{
				fadeOutAll();
			}
			else
			{
				fadeInAll();
			}

			m_lastEnableFadeing  = m_enableFading;
		}
	}

	void OnTriggerEnter (Collider other)
	{
		Fader fader = other.gameObject.GetComponent <Fader> ();
		if (fader)
		{
			fadeOut  (fader);
		}
	}

	void OnTriggerExit (Collider other)
	{
		Fader fader = other.gameObject.GetComponent <Fader> ();
		if (fader)
		{
				fadeIn (fader);

				m_fadedObjects.Remove (fader);
		}
	}

	void OnDisable ()
	{
		fadeInAll ();
	}

	void fadeInAll()
	{
		foreach (Fader fader in m_fadedObjects)
		{
			fadeIn (fader);
		}
	}

	void fadeOutAll ()
	{
		foreach (Fader fader in m_fadedObjects)
		{
			fadeOut (fader);
		}
	}

	void fadeOut (Fader fader)
	{
		if (m_faderCount.ContainsKey (fader))
		{
			++m_faderCount [fader];
		}
		else
		{
			m_faderCount [fader] = 1;
		}

		m_fadedObjects.Add (fader);

		if (m_enableFading)
		{
			fader.m_fadeIn = false;
			fader.m_minAlpha = m_minAlpha;
		}
	}

	void fadeIn (Fader fader)
	{
		--m_faderCount [fader] ;
		
		if (m_faderCount [fader]  == 0)
		{
			fader.m_fadeIn = true;
		}
	}
}
