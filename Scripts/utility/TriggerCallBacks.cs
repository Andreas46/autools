﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class ColliderEvent: UnityEvent <Collider> {}

public class TriggerCallBacks: MonoBehaviour
{
	public ColliderEvent m_triggerEnterCallBacks = new ColliderEvent();

	Collider m_col;
	public Collider Col { get { return m_col; } }

	[SerializeField] bool m_triggerOnlyOnce = true;
	bool m_hasTriggered = false;

	void Awake ()
	{
		m_col = GetComponent <Collider> ();
	}

	// Use this for initialization
	void OnTriggerEnter (Collider other)
	{
		if (m_hasTriggered && m_triggerOnlyOnce)
		{
			return;
		}

		m_hasTriggered = true;
		m_triggerEnterCallBacks.Invoke (other);

		//m_triggerEnterCallBacks.
	}
}
