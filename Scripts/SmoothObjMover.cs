﻿using UnityEngine;
using System.Collections;

using AuTools;

public class SmoothObjMover : MonoBehaviour 
{
	public bool m_enabled = false;

	public Transform m_travelTarget;
	public Transform TravelTarget { get { return m_travelTarget; } set { m_travelTarget = value; } }

	public float m_travelTime = 2.0f;
	float m_travelTimer;

	Vector3 m_startPos;
	Quaternion m_startRot;
	//Vector3 m_springVel;

	bool m_localTravel;
	Vector3 m_targetPos;
	Quaternion m_targetRot;

	static public SmoothObjMover getObjMover (GameObject obj)
	{
		// Get the already attached scripts (if one is attached).
		SmoothObjMover objMover = obj.GetComponent <SmoothObjMover> ();

		// If an instance of SmoothObjMover is not already attached then create one.
		if (objMover == null)
		{
			objMover = obj.AddComponent <SmoothObjMover>();

			Debug.Assert (objMover != null, "Can't create abject mover!?");
		}

		return objMover;
	}

	// Use this for initialization
	void Start () 
	{
		// "Disable" timer on startup.
		//m_travelTimer = m_travelTime;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_enabled && m_travelTimer < m_travelTime)
		{
			if (m_travelTarget != null)
			{
				// Update targets to reflect targets location.
				if (m_localTravel)
				{
					m_targetPos = m_travelTarget.localPosition;
					m_targetRot = m_travelTarget.localRotation;
				}
				else
				{
					m_targetPos = m_travelTarget.position;
					m_targetRot = m_travelTarget.rotation;
				}
			}

			m_travelTimer += Time.deltaTime;
			
			float travelRatio = m_travelTimer / m_travelTime;
			
			updateObjPos (travelRatio);
			updateObjRot (travelRatio);

			// Have we reached the end of our journey?
			if (m_travelTimer >= m_travelTime)
			{
				m_enabled = false;
			}
		}
	}

	void updateObjPos (float travelRatio)
	{
		Vector3 newPos = AMathVec3.smoothStep (m_startPos, m_targetPos, travelRatio);

		if (m_localTravel)
		{
			transform.localPosition = newPos;
		}
		else
		{
			transform.position = newPos;
		}
	}
	
	void updateObjRot (float travelRatio)
	{
		//Quaternion targetRot = transform.rotation;
		//Quaternion currentPos = m_cam.transform.rotation;
		
		Quaternion newRot = AMathVec3.smoothSlerp (m_startRot, m_targetRot, travelRatio);

		if (m_localTravel)
		{
			transform.localRotation = newRot;
		}
		else
		{
			transform.rotation = newRot;
		}
	}

	public void beginTravel (Vector3 targetPos, Quaternion targetRot)
	{
		//Vector3 localTargetPos;
		//Quaternion localTargetRot;

		m_enabled = true;
		m_travelTimer = 0.0f;

		m_localTravel = false;

		// Transform the given world co-ordinates to local co-ordinates.
		//if (transform.parent)
		//{
		//	localTargetPos = transform.parent.InverseTransformPoint (targetPos);
		//}
		//else
		//{
		//	localTargetPos = targetPos;
		//}

		//localTargetRot = transform.localRotation * (Quaternion.Inverse (transform.rotation) * targetRot);

		//beginTravelLocal (localTargetPos, localTargetRot);

		m_startPos = transform.position;
		m_startRot = transform.rotation;

		m_targetPos = targetPos;
		m_targetRot = targetRot;
	}

	public void beginTravelLocal (Vector3 targetPos, Quaternion targetRot)
	{
		m_enabled = true;
		m_travelTimer = 0.0f;

		m_localTravel = true;
		
		//Debug.Log ("begin travel");
		
		m_startPos = transform.localPosition;
		m_startRot = transform.localRotation;
		
		m_targetPos = targetPos;
		m_targetRot = targetRot;
	}

	//public void resetTravel ()
	//{
	//
	//}
}
